require_relative '../logitravel/offer'
require_relative '../logitravel/session'

module Logitravel

    class OfferHandler

      def initialize(env)
        DSL::CMS.set_conn(env)
        DSL::MongoDB.set_conn(env)
        @browser_session = Session.new
      end

      def update_cms_offer(mongo_id, cms_id)
        safe_method do
          offer = restore_offer(mongo_id)
          complete_offer(offer)
          offer.update_cms_offer(cms_id)
        end
      end

      def create_cms_offer(mongo_id)
        safe_method do
          offer = restore_offer(mongo_id)
          complete_offer(offer)
          offer.store_in_cms
        end
      end

      private

      def cleanup_session
        @browser_session.close if @browser_session
      end

      def safe_method
        begin
          yield
        rescue => e
          cleanup_session
          RollbarNotifier.error(e, offer.data)
        end
      end

      def complete_offer(offer)
        offer.complete_offer(@browser_session)
        cleanup_session
      end

      def restore_offer(mongo_id)
        mongo_bson_id = BSON::ObjectId.from_string(mongo_id)
        Offer.restore(Offer::MONGO_COL, '_id', mongo_bson_id)
      end

    end
end
