require_relative '../common/rollbar_notifier'
require_relative '../logitravel/logitravel_file'
require_relative '../logitravel/offer'

module Logitravel
  module OfferGeneratorUtils

    def init_offers(from, offer_type, env = :development)
      send("init_offers_from_#{from}", offer_type, env)
    end

    def init_offers_from_feed(offer_type, env = :development)
      l_file = LogitravelFile.new(offer_type)
      active_cruises = l_file.extract_data
      # Create internal offer object and filter the ones already published
      all_offers = create_offers(:feed, offer_type, active_cruises)
      all_offers
    end

    def init_offers_from_site(offer_type, env = :development)
      public_offers = @browser_session.get_public_offers_urls(offer_type)
      all_offers = create_offers(:site, offer_type, public_offers, env)
      all_offers
    end

    def create_offers(from, offer_type, public_offers, env = :development)
      offers = []
      public_offers.each do |public_offer_data|
        offers << Offer.new({ from: from, offer_type: offer_type, offer_data: public_offer_data }, env)
      end
      offers
    end

    def store_offers_in_mongo(offers)
      offers.each do |offer|
        # store a new entry in mongo
        offer.store_in_mongo
      end
    end

    def enhance_offers_with_web(browser_session, selected_offers)
      # Complete selected offers with screenshots and push to cms
      ready = []
      selected_offers.each do |offer|
        begin
          offer.capture_web_data(browser_session)
          ready << offer
        rescue => e
          p e
          RollbarNotifier.error(e, offer.data)
        end
      end
      ready
    end

    def evaluate_offers(list)
      offers_processed = []
      list.each do |offer|
        begin
          res = offer.evaluate
          offers_processed << [offer, res]
        rescue => e
          p e
          RollbarNotifier.error(e, offer.data)
        end
      end
      offers_processed
    end
  end
end
