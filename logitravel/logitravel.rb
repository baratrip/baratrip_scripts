require_relative '../common/rollbar_notifier'
require_relative '../logitravel/offer_generator'
require_relative '../logitravel/offer_handler'

module Logitravel
  include RollbarNotifier
end
