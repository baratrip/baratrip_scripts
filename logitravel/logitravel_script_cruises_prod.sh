#!/bin/bash
# Get 1 logitravel cruise, the cheapest
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching logitravel scripts in production mode"
ruby /home/apps/baratrip_scripts/logitravel/logitravel_launcher.rb production "get_new_cruise_sales"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished logitravel script. Elapsed time $(($ENDTIME - $STARTTIME))"
