require_relative '../common/dsl/dsl'
require_relative '../common/utils'
require_relative '../common/notification_manager'
require_relative '../logitravel/session'
require_relative '../logitravel/offer'
require_relative '../logitravel/offer_generator_utils'

module Logitravel

  class OfferGenerator
    include Common::Utils
    include NotificationManager
    include OfferGeneratorUtils

    def initialize(env = :development)
      @env = env
      DSL::CMS.set_conn(env)
      DSL::MongoDB.set_conn(env)
      @report_to = report_to(env)
    end

    def get_new_cruise_sales
      # Generate offers
      created_offers = create_and_publish_offers(:site, :cruise_sales)
      # Send email
      deliver_report("Logitravel - New Cruises Sales", created_offers)
      created_offers
    end

    def get_new_pack_sales
      # Generate offers
      created_offers = create_and_publish_offers(:site, :pack_sales)
      # Send email
      deliver_report("Logitravel - New Pack Sales", created_offers)
      created_offers
    end

    private

    def create_and_publish_offers(from, offer_type)
      @browser_session = Session.new
      # Get offer list from feed
      # Create internal offer object
      all_offers = init_offers(from, offer_type, @env)

      all_offers = enhance_offers_with_web(@browser_session, all_offers) if from == :feed

      # Store all data retrieved in mongo
      stored_offers = store_offers_in_mongo(all_offers)

      # Reject offer that we could not get a price
      filtered_offers = reject_no_price(stored_offers)

      # Sort them by price and choose cheapest and most expensive
      sorted_offers = sort_by_price(filtered_offers)

      # Classify between 'new' and 'found'
      evaluated_offers = evaluate_offers(sorted_offers)

      # Cleanup
      @browser_session.close

      evaluated_offers
    end

    def deliver_report(title, offers)
      report = create_report(offers)
      send_report( @report_to, title, report, [])
    end

    def create_report(list)
      msg = "<p><strong>Offers List (#{list.size})</strong></p>"
      list.each do |item|
        offer = item[0]
        result = item[1]
        data = offer.data

        msg << "<p>#{offer.data[:title]}: #{offer.data[:price]} <b>(#{result[:status].upcase})</b></p>"
        msg << "<p><span>Tracked url:</span> (#{offer.data[:track_uri]})</p>"
        msg << send("msg_for_#{result[:status]}", result)
        msg << "<p>------------------------------------------------------</p>"
      end
      msg
    end

    def msg_for_new(result)
      "<p><span>Action:</span> <a href='#{result[:create_link]}'>>> Click here to Create <<</a></p>"
    end

    def msg_for_found(result)
      list = result[:cms_offer_data].map{|k,v| "<li>#{k}:&nbsp;&nbsp;#{v}</li>"}
      "<p>
        <span>Prev Offer Info:</span>
        <ul>
          #{list.join('')}
        </ul>
      </p>
      <p><span>Action:</span> <a href='#{result[:update_link]}'>>> Click here to Update <<</a></p>"
    end

  end


end
