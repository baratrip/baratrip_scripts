#!/bin/bash
# Get 1 logitravel cruise, the cheapest
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching logitravel scripts in development mode"
ruby logitravel_launcher.rb development "get_new_pack_sales"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished logitravel script. Elapsed time $(($ENDTIME - $STARTTIME))"
