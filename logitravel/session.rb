require_relative '../common/browser'
require_relative '../common/screenshot_utils'

module Logitravel

  class Session < Browser::Session

    include Common::ScreenshotUtils

    SUBTITLE_PATH          = "//div[@id='headerPeticionCrucero']//div[contains(@class, 'info')]/p[contains(@class,'important')]"
    DATE_XPATH             = "//div[contains(@class,'bestPriceInfoStep2')]"

    SALES_URL = 'http://www.logitravel.com/viajes-chollos/'

    def get_public_offers_urls(offer_type)
      @page.visit SALES_URL
      send("get_public_offers_#{offer_type}")
    end

    def get_detailed_info_cruise_sales(url, hashed_url)
      @page.visit url
      benefits = @page.all(:xpath, "//*[@id='headerPeticionCrucero']/div[1]/div[1]/div[3]/ul/li").collect{|good| good.text}
      benefits += @page.all(:xpath, "//*[@id='headerPeticionCrucero']/div[1]/div[2]/div[2]/ul/li").collect{|good| good.text}
      screenshots = { main: take_screenshot_here("#{hashed_url}_main", '#headerPeticionCrucero') }
      score = @page.find(:xpath, "//*[@id='headerPeticionCrucero']/div[1]/div[2]/div[1]/a/p[1]").text
      { benefits: benefits, score: score, screenshots: screenshots }
    end

    def get_public_offers_cruise_sales
      @page.find(:xpath, "//a[@href='#pestanya_Cruises']").click
      sleep(4)
      offers = []
      elems = @page.all(:xpath, "//div[@id='pestanya_Cruises']/div")
      elems.each do |elem|
        @page.within(:xpath, elem.path) do
          price = @page.find(:xpath, ".//div[@class='contPrecio']/div/div[2]/a/span[2]/span").text
          link, teaser, itinerary, departures, duration, origin, cruise_company, cruise_ship = nil
          @page.within(:xpath, ".//div[@class='contInfo']") do
            link = @page.find(:xpath, "./a[1]")
            cruise_info = @page.find(:xpath, "./a[2]/span").text.split('|')
            cruise_company = cruise_info[0].strip
            cruise_ship = cruise_info[1].strip
            departures_info = @page.find(:xpath, "./div[1]/a/p").text
            departures = departures_info.match(/Fechas salida:(.*)/)[1].strip
            itinerary_info = @page.find(:xpath, "./div[2]/a/p").text
            itinerary = itinerary_info.match(/Itinerario:(.*)/)[1].strip
            days_info = @page.find(:xpath, "./p").text
            matches = days_info.match(/(\d*).*desde(.*)/)
            duration = matches[1]
            origin = matches[2]
          end
          clean_price = price.gsub('€','').strip
          link_url = link[:href]
          filename = hash_url(link_url)
          screenshots = { preview: take_screenshot_here("#{filename}_preview", "#pestanya_Cruises > div:nth-child(#{elems.index(elem) + 1})") }
          offers << { cruise_name: link.text, cruise_company: cruise_company, cruise_ship: cruise_ship, itinerary: itinerary, origin: origin, departures: departures, duration: duration, url: link_url, price: clean_price, screenshots: screenshots }
        end
      end
      offers
    end

    def get_detailed_info_pack_sales(url, hashed_url)
      @page.visit url
      # TODO remove duplicated screenshot. Change for different wait to scroll to element. without scroll, element not visible
      selector = '#pestanya_ > div:nth-child(2)'
      take_screenshot_here("#{hashed_url}_main", selector)
      sleep(2)
      main_scr = take_screenshot_here("#{hashed_url}_main", selector)
      screenshots = { main: main_scr }
      { screenshots: screenshots }
    end

    def get_public_offers_pack_sales
      @page.find(:xpath, "//a[@href='#pestanya_Packages']").click
      sleep(4)
      offers = []
      elems = @page.all(:xpath, "//div[@id='pestanya_Packages']/div")
      elems.each do |elem|
        @page.within(:xpath, elem.path) do
          price = @page.find(:xpath, ".//div[@class='contPrecio']/div/a/div/span/span[2]").text
          link, destination, origin, hotel, duration, info = nil
          @page.within(:xpath, ".//div[@class='contInfo']") do
            link = @page.find(:xpath, "./h2/a[1]")
            destination = link.text
            what = @page.find(:xpath, "./p/span[1]").text
            origin = what.match(/desde\s([\w*\s]*)$/)[1]
            hotel = @page.find(:xpath, "./p/span[2]").text
            info = @page.find(:xpath, "./p").text
            duration = info.match(/\d\s\S*\s\/\s\d\s\S*/)[0]
          end
          link_url = link[:href]
          clean_price = price.gsub('€','').strip
          filename = hash_url(link_url)
          screenshots = { preview: take_screenshot_here("#{filename}", "#pestanya_Packages > div:nth-child(#{elems.index(elem) + 1})") }
          offers << { destination: destination, origin: origin, hotel: hotel, duration: duration, url: link_url, price: clean_price, screenshots: screenshots }
        end
      end
      offers
    end

    def take_screenshot(url, filename, selector, optional = false)
      @page.visit url
      opts = {}
      if @page.has_css?(selector)
        take_screenshot_here(filename, selector)
      elsif !optional
        raise 'Cannot take requested screenshot'
      else
        nil
      end
    end

    # DEPRECATED used for feed offers
    def capture_web_data(url)
      @page.visit url

      sub = @page.find(:xpath, SUBTITLE_PATH).text

      date_span = @page.find(:xpath,DATE_XPATH).text
      date_match = date_span.match(/^.*(\d{2}\/\d{2}\/\d{4}).*$/)
      date = Date.parse(date_match[1]) unless date_match.nil?

      return {
          subtitle: sub,
          date:     date
      }
    end

  end



end
