require 'open-uri'
require 'json'

module Logitravel

    class LogitravelFile

      FILE_NAME = 'logitravel'
      FILE_TYPES = {
          circuit: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_150?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text',
          coast: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_184?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text',
          cruise: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_151?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text',
          cruise_sales: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_151?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text',
          #hotel: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_152?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text',
          pack: 'https://export.affiliate.logitravel.com/7d367ab0-0b26-4ca0-835e-a35a08a45be0/api/productdataexport_149?pid=26105&format=json&columns=Id_text,Name_text,Description_text,Price_text,Image_text,Url_text'

      }
      attr_accessor :offer_type, :filename, :filepath

      def initialize(offer_type)
        raise 'Offer type not available' if !FILE_TYPES.has_key?(offer_type)
        @offer_type = offer_type
        @filename = self.class.download(@offer_type)
        @filepath = self.class.filepath(@filename)
      end

      def self.download(offer_type)
        filename = Time.now.strftime("%Y_%m_%d_%H_%M_%S") + '_' + FILE_NAME + '_' + offer_type.to_s + '.json'
        File.open("/tmp/#{filename}", "wb") do |saved_file|
          open(FILE_TYPES[offer_type], "rb") do |read_file|
            saved_file.write(read_file.read)
          end
        end
        filename
      end

      def self.filepath(filename)
        '/tmp/' + filename
      end

      def extract_data
        file = File.read(@filepath)
        JSON.parse(file)['result_set']
      end

    end

end
