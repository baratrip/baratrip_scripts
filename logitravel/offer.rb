require 'digest'
require 'open-uri'

require_relative '../common/offer'
require_relative '../common/file_utils'

include FileUtils

module Logitravel

  class Offer < Common::Offer

    CMS_MODEL = 'offers'
    AFFILIATE = 'logitravel'
    MONGO_COL = :logitravel_offers

    def initialize(data, env = :development)
      @env = env
      @cms_model      = CMS_MODEL
      @affiliate      = AFFILIATE
      @mongo_col      = MONGO_COL

      if data.class == BSON::Document
        @data = data
      else
        send("initialize_from_#{data[:from].to_s}", data[:offer_type], data[:offer_data])
        @data.merge!({ offer_source: data[:from] })
      end
      @data
    end

    def initialize_from_site(offer_type, site_data)
      @cms_identifier = :landing_url
      landing_url = site_data[:url]
      price = site_data[:price]

      @data = {
        offer_type:   offer_type,
        price:        price.to_f,
        landing_url:  landing_url,
        hash_url:     hash_url(landing_url),
        track_uri:    URI.escape("http://ssl.affiliate.logitravel.com/ts/i2795678/tsc?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40497&rmd=3&trg=#{landing_url}?utm_source=Afiliado&utm_medium=22271%7C%7CN%2FA+-+UID+22271&utm_content=Baratrip&gclid=&gclsrc="),
        screenshots:  site_data[:screenshots] || {}
      }
      @data.merge!(send("site_#{offer_type}_data", site_data))
    end

    def site_pack_sales_data(site_data)
      destination = site_data[:destination]
      duration = site_data[:duration]
      origin = site_data[:origin]
      price = site_data[:price]
      title = "Vuelo más Hotel en #{destination} '#{duration}' desde #{price}€"
      seo_title = "Vuelo más Hotel en #{destination} desde #{price}€ - Baratrip.es"
      meta_description = "Disfruta de #{duration} días en #{destination} con esta oferta de vuelo más hotel en #{destination} desde tan solo #{price}€. Entra y conoce más detalles"
      permalink = "vuelo-mas-hotel-#{make_permalink(destination)}"

      {
        duration:     duration,
        destination:  destination,
        origin:       origin,
        title:        title,
        seo_title:    seo_title,
        teaser:       meta_description,
        meta_description: meta_description,
        tags:         ['vuelohotel', make_tag(destination)],
        permalink:    permalink,
        categories:   'vuelos-mas-hotel'
      }
    end

    def site_cruise_sales_data(site_data)
      cruise_name = site_data[:cruise_name]
      cruise_company = site_data[:cruise_company]
      cruise_ship = site_data[:cruise_ship]
      duration = site_data[:duration]
      itinerary = site_data[:itinerary]
      departures = site_data[:departures]
      origin = site_data[:origin]
      price = site_data[:price]
      title = "Crucero '#{cruise_name}' durante #{duration} días desde #{price}€"
      seo_title = "Crucero #{cruise_name} desde #{price}€ – Baratrip.es"
      meta_description = "Pasa #{duration} días en este maravilloso crucero #{cruise_name} con salida desde #{origin} desde #{price}€. Más detalles aquí"
      permalink = "crucero-#{make_permalink(cruise_name)}"

      tags = ['crucero'] + itinerary.split(',').collect{|leg| make_tag(leg)}.uniq

      {
        cruise_name:  cruise_name,
        cruise_company: cruise_company,
        cruise_ship:  cruise_ship,
        duration:     duration,
        itinerary:    itinerary,
        departures:   departures,
        origin:       origin,
        title:        title,
        seo_title:    seo_title,
        teaser:       meta_description,
        meta_description: meta_description,
        tags:         tags,
        permalink:    permalink,
        categories:   'cruceros'
      }
    end

    def initialize_from_feed(offer_type, feed_offer)
      @cms_identifier = :affiliate_offer_id

      @data = {
          title: feed_offer['Name_text'],
          teaser: feed_offer['Description_text'],
          description: feed_offer['Description_text'],
          affiliate_offer_id: feed_offer['Id_text'],
          track_uri: feed_offer['Url_text'],
          price: feed_offer['Price_text'].to_f,
          start_date: Date.today
      }
      # @data[:landing_url] = open(@data[:track_uri]).base_uri.to_s  Suddenly returning 500
      @data[:landing_url] = @data[:track_uri]
      @data[:hash_url] = hash_url(@data[:landing_url])

      @data
    end

    def evaluate
      cms_offers = self.class.find_by_tags(@data[:tags])
      if cms_offers.size > 0
        cms_offer = cms_offers[0]
        cms_offer_data = { edit_url: edit_offer_url(cms_offer['_id']) }
        ['_id', 'title', 'affiliate', 'price', 'posted_at', 'end_date', '_visible'].each{|attr| cms_offer_data[attr] = cms_offer[attr]}
        res = { status: :found, cms_offer_data: cms_offer_data, update_link: update_link(cms_offer['_id']) }
      else
        res = { status: :new, create_link: create_link }
      end
      res
    end

    def capture_web_data(browser_session)
      new_data = browser_session.capture_web_data(@data[:landing_url])
      @data[:end_date] = new_data[:date]
      @data[:title] = data[:title] + '. ' + new_data[:subtitle] + ' desde ' + @data[:price].to_s + '€'
    end

    def complete_offer(browser_session)
      send("complete_offer_#{@data[:offer_type]}", browser_session)
    end

    private

    def complete_offer_pack_sales(browser_session)
      new_data = browser_session.get_detailed_info_pack_sales(@data[:landing_url], @data[:hash_url])
      @data[:screenshots].merge!(new_data[:screenshots])
      desc = "<h2> Detalles de la oferta de vuelo más hotel a #{@data[:destination]}</h2>"
      desc += "<ul>"
      desc += "<li>Duración del viaje: #{@data[:duration]}</li>"
      desc += "<li>Salida desde: #{@data[:origin]}</li>"
      desc += "</ul>"
      desc += "<a href='#{@data[:track_uri]}' rel='nofollow' title='#{@data[:title]}' target='_blank'><img src='#{@data[:screenshots][:main]}' alt='#{@data[:title]}' /></a>"
      @data[:description] = desc
    end

    def complete_offer_cruise_sales(browser_session)
      new_data = browser_session.get_detailed_info_cruise_sales(@data[:landing_url], @data[:hash_url])
      @data[:screenshots].merge!(new_data[:screenshots])
      @data[:benefits] = new_data[:benefits]
      @data[:score] = new_data[:score]
      desc = "<a href='#{@data[:track_uri]}' rel='nofollow' title='#{@data[:title]}' target='_blank'><img src='#{@data[:screenshots][:preview]}' alt='#{@data[:title]}' /></a>"
      desc += "<h2>Detalles del crucero '#{@data[:cruise_name]}' desde #{@data[:price]}€</h2>"
      desc += "<ul>"
      desc += "<li>Duración del crucero: #{@data[:duration]} días</li>"
      desc += "<li>Fechas de salida: #{@data[:departures]}</li>"
      desc += "<li>Itinerario: #{@data[:itinerary]}</li>"
      desc += "<li>Navío: #{@data[:cruise_ship]}</li>"
      desc += "<li>Puerto de salida: #{@data[:origin]}</li>"
      desc += "<li>Puntuación: #{@data[:score]}</li>"
      desc += "</ul>"
      if @data[:benefits] && @data[:benefits].size > 0
        desc += "<h3>Recomendaciones de la oferta y ventajas:</h3>"
        desc += "<ul>"
        @data[:benefits].each do |benefit|
          desc += "<li>#{benefit}</li>"
        end
        desc += "</ul>"
      end
      desc += "<a href='#{@data[:track_uri]}' rel='nofollow' title='#{@data[:title]}' target='_blank'><img src='#{@data[:screenshots][:main]}' alt='#{@data[:title]}' /></a>"
      @data[:description] = desc
    end

  end


end
