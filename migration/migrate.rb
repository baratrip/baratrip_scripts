require 'mongo'

CONTENT_TYPE_POST = BSON::ObjectId.from_string("5653704c0f64e321fe000009")
CONTENT_TYPE_OFFER = BSON::ObjectId.from_string("5653704e0f64e321fe000024")
CONTENT_TYPE_PHOTO = BSON::ObjectId.from_string("5653704e0f64e321fe000036")
CONTENT_TYPE_CATEGORY = BSON::ObjectId.from_string("5653704e0f64e321fe000033")

POST_TYPE_OFFER = BSON::ObjectId.from_string("5653704c0f64e321fe000010")

conn = Mongo::Client.new('mongodb://127.0.0.1:27017/baratrip-dev')
coll = conn['locomotive_content_entries']


posts = coll.find("content_type_id": CONTENT_TYPE_POST, post_type_id: POST_TYPE_OFFER).to_a

p "Number of posts with post_type: 'offer': #{posts.size}"

posts.each do |post|
  post_id = post["_id"]

  # Link offers with categories. It's many_to_many so relation is stored in both sides
  category_ids = post["category_ids"]
  coll.update_many(
    { "content_type_id": CONTENT_TYPE_OFFER, post_id: post_id },
    { "$set" => {
      "category_ids" => category_ids,
      "body" => post["body"],
      "_slug" => post["_slug"],
      "posted_at" => post["posted_at"]
      }
    }
  )
  offers = coll.find("content_type_id": CONTENT_TYPE_OFFER, post_id: post_id).to_a
  offer_ids = offers.collect{|o| o["_id"]}
  coll.update_many( { "_id": {"$in": category_ids}, "content_type_id": CONTENT_TYPE_CATEGORY }, { "$addToSet" => { "offer_ids" => { "$each": offer_ids } } } )

  # Link offers with photos. It's one_to_many, relation is stored in photos
  offer_id = offer_ids.last # Pointing the photo to the latest offer
  coll.update_many( { "content_type_id": CONTENT_TYPE_PHOTO, post_id: post_id }, { "$set" => { "offer_id" => offer_id } } )
end


# db.getCollection('locomotive_content_entries').find({post_id: {$exists: false}, content_type_id: ObjectId("5653704e0f64e321fe000024")})
