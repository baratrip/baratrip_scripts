require_relative '../common/browser'
require_relative '../common/screenshot_utils'

module Booking

  class Session < Browser::Session

    include Common::ScreenshotUtils

    def get_cheapest_hotel(dest, checkin = Date.today, checkout = Date.today + 2)
      go_to_result(dest, checkin, checkout) do
        @page.find(:xpath, "//li[contains(@class, 'sort_price')]/a").click
        sleep(5)
        @page.within(:xpath, "//div[@id='ajaxsrwrap']") do
          take_screenshot_here('test', "#hotellist_inner > div:nth-child(1)")
        end
      end
    end

    def get_cheapest_recommended_hotel(dest, checkin = Date.today, checkout = Date.today + 2)
      go_to_result(dest, checkin, checkout) do
        @page.within(:xpath, "//div[@id='ajaxsrwrap']") do
          # TODO iterate all divs since not all have price
          @page.all("#hotellist_inner > div").each do |div|
            p div.text
            p div.has_xpath?(".//strong[contains(@class, 'price')]/b")
          end

          elems = @page.all(:xpath, "//strong[contains(@class, 'price')]/b").collect.with_index{ |elem, i| { price: elem.text.gsub!('€', '').strip!, elem: elem, text: elem.text, position: i  } }
          elems.sort!{|offer, offerb| offer[:price].to_i <=> offerb[:price].to_i }
          p elems.collect{|e| e[:price]}
          cheapest = elems.first
          take_screenshot_here('test', "#hotellist_inner > div.sr_item:nth-child(#{cheapest[:position] + 1})")
          #hotellist_inner > div:nth-child(15)
        end
      end
    end

    def go_to_result(dest, checkin, checkout, &block)
      p "Search for #{dest}. From #{checkin.to_s} to #{checkout.to_s}"
      begin
        set_headers
        @page.visit('http://www.booking.com/index.es.html')
        fill_first_search(dest)
        fill_results_search(dest, checkin, checkout)
        clean_page
        yield
      rescue => e
        @page.save_screenshot('tmp/error.png')
        p e
        p e.backtrace
        File.open("tmp/error.html", "w") {|f| f.write(@page.html) }
      end
    end

    private

    def set_headers
      if @driver == :poltergeist
        @page.driver.headers = { "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }
        @page.driver.add_header("Cookie", "header_signin_prompt=1; zz_cook_tms_seg1=1; zz_cook_tms_seg3=8; has_preloaded=1; _gat=1; _ga=GA1.2.1427917785.1464436892; BJS=-; b=%7B%22countLang%22%3A3%7D; utag_main=v_id:0154f73da2f4002c36c3f9a5ee1605079002407100bd0$_sn:1$_ss:0$_pn:3%3Bexp-session$_st:1464439379560$ses_id:1464436892404%3Bexp-session; _tq_id.TV-273681-1.3b4c=c8721f979c1b9924.1464436893.0.1464437580..; lastSeen=0; bkng=11UmFuZG9tSVYkc2RlIyh9YWJdm48m5cJDWuLLIYaigN5EqfxHAHCK8MlAVkfVq6xEqoOhLBsVD5ePi5mfDB3pbfIrMsGFbqgyu2y3P69Etcl1LGttMDeem0Xdf0znX6X92zI%2FczngW3NHVmhwltglFvMljxJzrW0N8rIkvcZIf51FJ7XTlC05dFqYnMzo2d2jUnwxsSRLZYg%3D", permanent: false)
        @page.driver.add_header("Host", "www.booking.com", permanent: false)
      else
        p "Driver is #{@driver}. No need to set headers"
      end
    end

    def select_from_chosen(value, from)
      @page.execute_script("$(\"select[name='#{from}']\").val('#{value}')")
      @page.execute_script("$(\"select[name='#{from}']\").change()")
    end

    def select_dates(checkin, checkout)
      checkin_day = checkin.day.to_s
      checkin_year = checkin.year
      checkin_month = checkin.month
      checkin_year_month = "#{checkin.year}-#{checkin.month}"
      checkout_day = checkout.day.to_s
      checkout_year_month = "#{checkout.year}-#{checkout.month}"
      checkout_year = checkout.year
      checkout_month = checkout.month

      if @page.has_xpath?(".//input[@name='checkin_year']")
        p "Filling dates method 1"
        # @page.find(:xpath, ".//input[@name='checkin_monthday']").set checkin_day
        # @page.find(:xpath, ".//input[@name='checkin_month']").set checkin_month
        # @page.find(:xpath, ".//input[@name='checkin_year']").set checkin_year
        @page.execute_script("$(\"input[name='checkin_monthday']\").val('checkin_day')")
        @page.execute_script("$(\"input[name='checkin_month']\").val('checkin_month')")
        @page.execute_script("$(\"input[name='checkin_year']\").val('checkin_year')")
        # @page.fill_in 'checkin_monthday', :with => checkin_day
        # @page.fill_in 'checkin_month', :with => checkin_month
        # @page.fill_in 'checkin_year', :with => checkin_year
        @page.fill_in 'checkout_monthday', :with => checkout_day
        @page.fill_in 'checkout_month', :with => checkout_month
        @page.fill_in 'checkout_year', :with => checkout_year
      elsif @page.has_select?('checkin_monthday')
        p "Filling dates method 2"
        select_from_chosen(checkin_day, 'checkin_monthday')
        select_from_chosen(checkin_year_month, 'checkin_year_month')
        select_from_chosen(checkout_day, 'checkout_monthday')
        select_from_chosen(checkout_year_month, 'checkout_year_month')
      else
        p "Filling dates method 3"
        @page.execute_script("$('#frm').append('<input type=\"hidden\" name=\"checkin_monthday\" value=\"#{checkin_day}\" />');")
        @page.execute_script("$('#frm').append('<input type=\"hidden\" name=\"checkin_year_month\" value=\"#{checkin_year_month}\" />');")
        @page.execute_script("$('#frm').append('<input type=\"hidden\" name=\"checkout_monthday\" value=\"#{checkout_day}\" />');")
        @page.execute_script("$('#frm').append('<input type=\"hidden\" name=\"checkout_year_month\" value=\"#{checkout_year_month}\" />');")
      end
    end

    def select_destination
      @page.save_screenshot('tmp/destination.png')
      xpath = '//*[@id="disambBlock_city"]/div[2]/div[1]/div[3]/a'
      if @page.has_xpath?(xpath)
        @page.find(:xpath, xpath).click
      end
    end

    def select_cheapest_result

    end

    def clean_page
      @page.all('span.close_inspire_filter_block').each{ |elem| elem.click }
    end

    def fill_first_search(dest)
      @page.within(:xpath, "//div[contains(@class, 'sb-searchbox__outer')]/form[@id='frm'][contains(@class, '-has-emk-subscribe-bar-below')]") do
        @page.find(:xpath, ".//input[@id='ss']").set dest
        @page.find_button('Buscar').click
      end
    end

    def fill_results_search(dest, checkin, checkout)
      @page.within(:xpath, "//form[@id='frm']") do
        @page.find(:xpath, ".//input[@id='ss']").set dest
        sleep(2)
        if @page.has_xpath?(".//ul[contains(@class, 'c-autocomplete__list')]/li")
           @page.find(:xpath, ".//ul[contains(@class, 'c-autocomplete__list')]/li[1]").click
           sleep(2)
        end
        select_dates(checkin, checkout)
        @page.find_button('Buscar').click
      end

    end

  end

end