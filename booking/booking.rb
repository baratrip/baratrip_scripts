require_relative '../booking/session'

module Booking

  class Script

    def get_hotel(dest, checkin, checkout)
      @session = Session.new
      @session.get_cheapest_hotel(dest, checkin, checkout)
    end

  end

end