module Common

  module Utils
    require "i18n"

    def make_permalink(text)
      return if !text
      res = I18n.transliterate(text)
      return res.downcase.strip.gsub(' ', '-')
    end

    def make_tag(text)
      return if !text
      res = I18n.transliterate(text)
      res.gsub!(/\.{3}/,'')
      res.gsub!(/\(.*\)*/,'')
      return res.downcase.strip
    end

    def hash_url(bare_url)
      Digest::SHA256.hexdigest bare_url
    end

    def track_tradedoubler_url(bare_url, affiliation_id, product_id)
      "http://clk.tradedoubler.com/click?p(#{affiliation_id})a(2443327)g(#{product_id})url(#{bare_url})"
    end

    def reject_no_price(offers)
      offers.reject!{|o| o.data[:price].nil?}
      offers
    end

    def sort_by_price(offers)
      offers.each{|offer|  offer.data[:price] = price_to_f(offer.data[:price]) }
      offers.sort!{|offer, offerb| offer.data[:price] <=> offerb.data[:price] }
      offers
    end

    def price_to_f(price)
      if price.class.to_s === 'String'
        price.gsub!('€','')
        price.gsub!('.','')
        price.gsub!(',','.')
        price = price.to_f
      end
      price
    end

    def filter_published_offers(all_offers)
      new_offers, errors = [], []

      # Get data from all public offers
      all_offers.each do |offer|
         # exclude the ones already stored by url in cms
          begin
            unless offer.already_in_cms?
              new_offers << offer
            end
          rescue => e
            errors << [offer.url, e]
          end
      end

      {
          all: all_offers,
          new: new_offers,
          errors: errors
      }
    end

  end

end
