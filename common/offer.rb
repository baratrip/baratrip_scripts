require_relative '../common/baratrip_cms'
require_relative '../common/utils'
require_relative '../common/dsl/dsl'

module Common

  class Offer

    include BaratripCMS
    include Utils
    include DSL

    attr_accessor :data, :affiliate, :cms_model, :mongo_col

    def []
      @data
    end

    def data
      @data
    end

    def [](attr)
      @data[attr]
    end

    def url
      @data[:landing_url]
    end

    def hashed_url
      @data[:hash_url]
    end

    def capture_web_data(browser_session)
      data = browser_session.capture_web_data(@data[:landing_url])
      @data.merge!(data)
    end

    def create_link
      server_url = get_server_address
      "#{server_url}/offer/#{@mongo_id}/create?offer_type=#{@affiliate}"
    end

    def update_link(cms_offer_id)
      server_url = get_server_address
      "#{server_url}/offer/#{@mongo_id}/update/#{cms_offer_id}?offer_type=#{@affiliate}"
    end

    ### CMS ###

    def self.find_in_cms(identifier, value)
      query = { "#{identifier.to_s}": value , '_visible' => true }
      DSL::CMS.find('offers', query)
    end

    def self.update_in_cms(cms_id, identifier, value)
      query = { "#{identifier.to_s}": value }
      DSL::CMS.update('offers', cms_id, query)
    end

    def self.find_by_tags(tags = [])
      query = { 'tags' => { '$all': tags }, '_visible' => true }
      DSL::CMS.find('offers', query)
    end

    def already_in_cms?
      raise "CMS model not defined for #{self.class}" if @cms_model.nil?
      raise "CMS identifier not defined for #{self.class}" if @cms_identifier.nil?
      raise "CMS identifier not set for #{self}" if self[@cms_identifier].nil?
      DSL::CMS.already_in_cms?(@cms_model, @cms_identifier.to_s, self[@cms_identifier])
    end

    def update_cms_offer(cms_id)
      DSL::CMS.update(@cms_model, cms_id, map_data_to_cms)
      edit_offer_url(cms_id)
    end

    def store_in_cms
      raise "CMS model not defined for #{self.class}" if @cms_model.nil?
      id = DSL::CMS.store_in_cms(@cms_model, map_data_to_cms)
      @data[:cms_id] = id
      @data[:cms] = edit_offer_url(id)
    end

    def upload_photo_to_cms(photo, caption)
      DSL::CMS.upload_photo(@data[:cms_id], photo, caption)
    end

    def map_data_to_cms
      {
          title: @data[:title],
          seo_title: @data[:seo_title],
          teaser: @data[:teaser],
          description: @data[:description],
          meta_description: @data[:meta_description],
          affiliate: @affiliate,
          affiliate_offer_id:  @data[:affiliate_offer_id],
          track_uri: @data[:track_uri],
          landing_url: @data[:landing_url],
          price: @data[:price],
          tags: @data[:tags],
          start_date: @data[:start_date],
          end_date: @data[:end_date],
          categories: @data[:categories],
          _visible: false,
          _slug: @data[:permalink],
          posted_at: Date.today
      }
    end

    ### MONGO ###

    def self.restore(mongo_col, key, value)
      data = restore_from_mongo(mongo_col, key, value)
      self.new(data)
    end

    def self.restore_from_mongo(mongo_col, key, value)
      raise "MongoDB collection not defined for #{self.class}" if mongo_col.nil?
      DSL::MongoDB.find(mongo_col, key, value)
    end

    def already_in_mongo?
      raise "MongoDB collection not defined for #{self.class}" if @mongo_col.nil?
      DSL::MongoDB.already_in_mongo?(@mongo_col, 'landing_url', self.url)
    end

    def store_in_mongo
      raise "MongoDB collection not defined for #{self.class}" if @mongo_col.nil?
      res = DSL::MongoDB.store(@mongo_col, self.data)
      @mongo_id = res.inserted_id.to_s
    end

    def enrich_from_mongo
      data = self.class.restore_from_mongo(@mongo_col, 'landing_url', self.url)
      @data[:price] = data[:price]
      @data[:title] = data[:title]
      @data[:teaser] = data[:teaser]
      @data[:start_date] = data[:start_date]
      @data[:end_date] = data[:end_date]
      @data[:description] = data[:description]
    end

    private

    def get_server_address
      file_path = File.expand_path('../config/server.json', File.dirname(__FILE__))
      if File.exist?(file_path) # TODO remove when automatic deploy is setup and mongodb.json can be replaed in deploy time
        JSON.parse(File.read(file_path))[@env.to_s]
      else
        'http://localhost:4567'
      end
    end

    def take_screenshot(browser_session, url, filename, selector, optional = false)
      browser_session.take_screenshot(url, filename, selector, optional)
    end

    def upload_screenshot(local_path, filename)
      if local_path
        file = File.new(local_path)
        raise "Screenshot #{filename} was not properly created for #{url}:  #{local_path}" if file.size < 1000
        s3_path = FileUtils.uploadToS3(local_path, filename)
        {
            local: local_path,
            s3: s3_path
        }
      else
        {
          local: nil,
          s3: nil
        }
      end
    end

  end

end
