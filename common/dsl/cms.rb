require 'locomotive/coal'

module DSL

  module CMS

    CONFIG_PATH =  File.expand_path('../../config/cms.json', File.dirname(__FILE__))

    def self.set_conn(env = :development)
      env = env.to_sym if env
      raise "Environment is invalid" if env.nil? || ![:development, :production, :test].include?(env)
      @@config = JSON.parse(File.read(CONFIG_PATH))[env.to_s]
      @@client = Locomotive::Coal::Client.new( @@config["host"], { email: @@config["email"], api_key: @@config["api_key"] } )
      @@site_client = @@client.scope_by(@@config["handle"])
    end

    def self.already_in_cms?(model, key, value)
      begin
        params = { "#{key}": value }
        res = find(model, params)
        cms_obj = res[0]
        return !cms_obj.nil?
      rescue => e
        p "Error searching in cms #{e.message}"
        p e.backtrace
        return true
      end
    end

    def self.find(model, params)
      return @@site_client.contents.send(model).all(params).collect{|content| content.attributes}
    end

    def self.store_in_cms(model, cms_data)
      obj = @@site_client.contents.send(model).create(cms_data)
      obj.attributes['_id']
    end

    def self.update(model, id, cms_data)
      @@site_client.contents.send(model).update(id, cms_data)
    end

    def self.upload_photo(offer_id, photo, caption)
      @@site_client.contents.send('photos').create({
        "caption"=> caption,
        "file"=> Locomotive::Coal::UploadIO.new(photo.location),
        "offer"=> offer_id, 
        "_visible"=> "1"
        })

    end

  end


end
