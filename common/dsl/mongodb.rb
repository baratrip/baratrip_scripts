require 'mongo'

module DSL
  module MongoDB

    CONFIG_PATH = File.expand_path('../../config/mongodb.json', File.dirname(__FILE__))

    def self.set_conn(env = :development)
      @@env = env
      if env == :test # TODO remove when automatic deploy is setup and mongodb.json can be replaed in deploy time
        db = "baratrip-scripts-test"
        url = "127.0.0.1:27017"
      else
        creds = JSON.parse(File.read(CONFIG_PATH))[env.to_s]
        db = creds['db']
        url = creds['url']
      end

      @@conn = Mongo::Client.new([ url ], :database => db)
    end

    def self.already_in_mongo?(collection, key, value)
      begin
        mongo_offer = find(collection, key, value)
        return !mongo_offer.nil?
      rescue => e
        p e
        return false
      end
    end

    def self.find(collection, key, value)
      @@conn[collection].find("#{key}": value).to_a.last
    end

    def self.find_by_hash(collection, hash)
      @@conn[collection].find(hash)
    end

    def self.store(collection, data)
      @@conn[collection].insert_one(data)
    end

    def self.clean
      raise "Cannot perform action for #{env} environment" if @@env != :test
      database = @@conn.database
      database.collections.each{ |coll| coll.drop }
    end
  end

end
