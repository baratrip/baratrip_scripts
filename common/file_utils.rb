require 'zip'
require 'aws-sdk'
require 'json'

module FileUtils

  CONFIG_PATH = File.expand_path('../config/aws.json', File.dirname(__FILE__))


  def self.uploadToS3(file_path, filename)
    creds = JSON.parse(File.read(CONFIG_PATH))

    Aws.config.update({
        region: 'eu-west-1',
        credentials: Aws::Credentials.new(creds['AccessKeyId'], creds['SecretAccessKey'])
    })

    s3 = Aws::S3::Resource.new
    obj = s3.bucket('baratrip').object("screenshots/#{filename}")
    obj.upload_file(file_path, acl:'public-read')
    return obj.public_url
  end

  def self.zipFiles(input_filenames, output_filename)

    zipfile_name = "tmp/#{output_filename}.zip"

    Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
      input_filenames.each do |filename|
        # Two arguments:
        # - The name of the file as it will appear in the archive
        # - The original file, including the path to find it
        zipfile.add(filename, 'tmp/' + filename)
      end
    end

    return zipfile_name
  end

  def self.unzipFile(input_filename)
    data = ''
    Zip::File.open(input_filename) do |zip_file|
      # Find specific entry
      entry = zip_file.glob('*.csv').first
      data = entry.get_input_stream.read
      return data
    end
  end



end