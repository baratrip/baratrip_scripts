require 'mail'

module NotificationManager

  CONFIG_PATH = File.expand_path('../config/smtp.json', File.dirname(__FILE__))

  def report_to(env)
    env.to_s == 'production' ? 'info@baratrip.es' : 'javier.jimenez.villarreal@gmail.com'
  end

  def send_report(to, subject, report, attachments)
    if @env && @env == :test
      true
    else
      creds = JSON.parse(File.read(CONFIG_PATH))

      config = {
          :address              => creds['address'],
          :port                 => creds['port'],
          :domain               => creds['domain'],
          :user_name            => creds['user_name'],
          :password             => creds['password'],
          :authentication       => :login,
          :enable_starttls_auto => true
      }

      Mail.defaults do
        delivery_method :smtp, config
      end

      Mail.deliver do
        to to
        from 'javier.jimenez.villarreal@gmail.com'
        subject "Baratrip Scripts: #{subject}"
        html_part do
          content_type 'text/html; charset=UTF-8'
          body report
        end
        attachments.each do |attachment|
          add_file attachment
        end
      end
    end
  end

end
