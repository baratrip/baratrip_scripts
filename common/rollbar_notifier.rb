require 'rollbar'
require 'json'


module RollbarNotifier

  CONFIG_PATH = File.expand_path('../config/rollbar.json', File.dirname(__FILE__))

  if File.exist?(CONFIG_PATH)
    Rollbar.configure do |config|
      creds = JSON.parse(File.read(CONFIG_PATH))
      config.access_token = creds['access_token']
      # Other Configuration Settings
    end
  else
    p 'No Rollbar config file'
  end

  def self.root
    File.dirname __dir__
  end

  def self.error(e, data = {})
    Rollbar.error(e, data)
  end

  def self.info(e, data = {})
    Rollbar.info(e, data)
  end

  def self.debug(e, data = {})
    Rollbar.debug(e, data)
  end

end