module BaratripCMS

  def edit_promocode_url(promocode_id)
    return "http://baratrip.es/locomotive/nely-moss-8000/content_types/promocodes/entries/#{promocode_id}/edit"
  end

  def edit_offer_url(offer_id)
    return "http://baratrip.es/locomotive/nely-moss-8000/content_types/offers/entries/#{offer_id}/edit"
  end

end