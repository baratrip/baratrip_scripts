module FolderUtils

  def self.setup_folder(folder = '/tmp/')
    delete_folder(folder) if Dir.exist?(folder)
    Dir.mkdir(folder)
  end


  def self.setup_tmp_folder(folder = '/tmp/')
    delete_folder(folder) if Dir.exist?(folder)
    Dir.mkdir(folder)
    yield
    delete_folder(folder)
  end

  def self.delete_folder(folder = '/tmp/')
    Dir.foreach(folder) do |f|
      fn = File.join(folder, f)
      if f == '.' || f == '..'
        next
      elsif File.directory?(fn)
        delete_folder(fn)
      else
        File.delete(fn)
      end
    end
    Dir.delete(folder)
  end


end