require 'uri'
require 'net/http'

require_relative '../common/file_utils'
include FileUtils

module Common
	class LocalPhoto

		attr_accessor :filename, :location

		def	initialize(filename)
			@filename = filename
			@location = "/tmp/#{filename}"
		end

		def	import(url)
			host = URI.parse(url).host

			Net::HTTP.start(host) { |http|
				resp = http.get(url)

				open(@location ,"wb"){ |file|
					file.write(resp.body)
				}

			}
		end

	end
end
