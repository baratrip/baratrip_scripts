require_relative '../common/file_utils'
include FileUtils

module Common
  module ScreenshotUtils

    def take_screenshot(url, filename, selector, optional = false)
      @page.visit url
      local_path = save_screenshot(filename, selector)
      path_info = upload_screenshot(local_path, filename)
      return path_info
    end

    def take_screenshot_here(filename, selector, upload = true)
      local_path = save_screenshot(filename, selector)
      if upload
        path_info = upload_screenshot(local_path, filename)
        path_info[:s3]
      else
        true
      end
    end

    def save_screenshot(filename, selector)
      opts = { quality: 100, selector: selector }
      path = "tmp/#{filename}.png"
      @page.save_screenshot(path, opts)
    end

    def upload_screenshot(local_path, filename)
      if local_path
        file = File.new(local_path)
        raise "Screenshot #{filename} was not properly created:  #{local_path}" if file.size < 1000
        s3_path = FileUtils.uploadToS3(local_path, filename)
        {
            local: local_path,
            s3: s3_path
        }
      else
        {
            local: nil,
            s3: nil
        }
      end
    end
  end
end
