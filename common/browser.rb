require 'capybara'
require 'capybara/poltergeist'
require 'capybara/dsl'

require_relative '../common/utils'
require_relative '../common/screenshot_utils'

module Browser

  Capybara.register_driver(:poltergeist) { |app| Capybara::Poltergeist::Driver.new(app, js_errors: false) }
  # Capybara.javascript_driver = :poltergeist
  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

  class Session
    include Common::Utils

    def initialize(driver = :poltergeist)
      @driver = driver
      @page = Capybara::Session.new driver
    end

    def close
      @page.driver.quit
    end

    def page
      @page
    end

  end
end
