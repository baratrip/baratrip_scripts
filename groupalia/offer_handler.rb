require_relative '../groupalia/offer'
require_relative '../groupalia/session'

module Groupalia

    class OfferHandler

      def initialize(env)
        DSL::CMS.set_conn(env)
        DSL::MongoDB.set_conn(env)
        @browser_session = Session.new
      end

      def update_cms_offer(mongo_id, cms_id)
        safe_method do
          offer = restore_offer(mongo_id)
          take_screenshots(offer)
          offer.update_cms_offer(cms_id)
        end
      end

      def create_cms_offer(mongo_id)
        safe_method do
          offer = restore_offer(mongo_id)
          take_screenshots(offer)
          cms_url = offer.store_in_cms
          offer.add_photo
          cms_url
        end
      end

      private

      def cleanup_session
        @browser_session.close if @browser_session
      end

      def safe_method
        begin
          yield
        rescue => e
          cleanup_session
          RollbarNotifier.error(e, offer.data)
        end
      end

      def take_screenshots(offer)
        offer.take_screenshots(@browser_session)
        cleanup_session
      end

      def restore_offer(mongo_id)
        mongo_bson_id = BSON::ObjectId.from_string(mongo_id)
        Offer.restore(Offer::MONGO_COL, '_id', mongo_bson_id)
      end

      # Complete selected offers with screenshots and push to cms
      # @offers_created = enhance_offers_with_web(@browser_session, selected_offers)
    end
end
