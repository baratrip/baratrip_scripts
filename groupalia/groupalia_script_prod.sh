#!/bin/bash
# Get two groupalia offers. cheapest and most expensive
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching groupalia script in production mode"
ruby /home/apps/baratrip_scripts/groupalia/groupalia_launcher.rb production "get_daily_two_offers"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished groupalia script. Elapsed time $(($ENDTIME - $STARTTIME))"
