require_relative '../common/rollbar_notifier'
require_relative '../groupalia/offer_generator'
require_relative '../groupalia/offer_handler'

module Groupalia
  include RollbarNotifier
end
