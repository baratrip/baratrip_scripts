require 'csv'
require_relative './groupalia'
require_relative '../common/utils'
require_relative '../common/offer'
include Groupalia
include Common
include Common::Utils

# @browser_session = Session.new
# CSV.open("tags.csv", "wb") do |csv|
#   CSV.foreach("myRecords.csv") do |row|
#     id = row[0]
#     url = row[1]
#     p url
#     destination = @browser_session.get_destination_from_final_url(url)
#     p destination
#     tags = [id]
#     tags = destination ? tags + destination.split(',').collect{|leg| make_tag(leg)}.uniq : tags
#     p tags
#     csv << tags
#   end
# end
DSL::CMS.set_conn(:development)
CSV.foreach("tags.csv") do |row|
    id = row[0]
    id.gsub!('ObjectId(','')
    id.gsub!(')','')
    tags = ['escapadas']
    next if row.size < 2
    next if row.join().include?('localizaciones')
    tags = tags + row[1..(row.size - 1)]
    cms_offers = Offer.find_in_cms('_id', id)
    unless cms_offers.empty?
      p id
      p tags
      Offer.update_in_cms(id, :tags, tags)
    end
end
