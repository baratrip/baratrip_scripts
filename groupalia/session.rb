require_relative '../common/browser'
require_relative '../common/utils'
require_relative '../common/screenshot_utils'

module Groupalia

  class Session < Browser::Session
    include Common::Utils
    include Common::ScreenshotUtils

    TRAVEL_OFFERS_URL       = "http://es.groupalia.com/ofertas-viajes/"
    H1_XPATH                = "//h1"
    PRICE_XPATH             = "//ul[@class='caja related-products']/li[1]/p/span[@class='price']"
    PRICE_XPATH_2           = "//ul[@class='related-products']/li[1]/p/span[@class='price']"
    DATE_XPATH              = "//div[@class='sections']//div[@id='highlighted']//p[1]//strong"
    TEASER_XPATH            = "//div[@class='sections']//div[@id='highlighted']//h3[1]"
    CLOSE_COOKIE_LINK_XPATH = "//a[@class='closeCookie'][1]"
    CLOSE_MODAL_LINK_XPATH  = "//a[@class='closeNyroModal'][1]"

    def take_screenshot(url, filename, selector, optional = false)
      @page.visit url
      clean_cookies_alert
      opts = {}
      if @page.has_css?(selector)
        take_screenshot_here(filename, selector)
      elsif !optional
        raise 'Cannot take requested screenshot'
      else
        nil
      end
    end

    def get_public_offers
      @page.visit TRAVEL_OFFERS_URL
      get_more_offers
      offers = []
      elems = @page.all(:xpath, "//ul[@id='your-deals']/li[@class='flash']")
      elems.each do |elem|
        @page.within(:xpath, elem.path) do
          destination_xpath = "./div[@class='dealTitle']/p[@class='product-info']/strong[contains(@class,'district')]"
          if @page.has_xpath?(destination_xpath)
            link = @page.find(:xpath, "./a[1]")
            title = @page.find(:xpath, "./div[1]/a").text
            destination = @page.find(:xpath, destination_xpath).text
            next if destination.include?('localizaciones')
            price = @page.find(:xpath, "./p[@class='price_grp']/span[contains(@class,'price')]").text
            clean_price = price.gsub('€','').strip
            offers << { url: link[:href], title: title, destination: destination, price: clean_price }
          end
        end
      end
      offers
    end

    def get_more_offers
      if @page.has_css?('a#more-your-deals')
        @page.find_link('more-your-deals').trigger('click')
        sleep(5)
        get_more_offers
      else
        true
      end
    end

    def get_destination_from_final_url(url)
      sleep(3) # to avoid connection limits
      @page.visit url
      clean_cookies_alert
      destination_xpath = "//p[@class='product-info txt-mb']/strong[@class='locate']"
      if @page.has_xpath?(destination_xpath)
        @page.find(:xpath, destination_xpath).text
      else
        nil
      end
    end

    def get_data_from_final_url(url)
      sleep(3) # to avoid connection limits
      @page.visit url
      clean_cookies_alert

      date = @page.all(:xpath, DATE_XPATH)
      date_from = Date.parse(date[0].text)
      date_to = Date.parse(date[1].text)

      teaser = @page.find(:xpath, TEASER_XPATH).text
      photo_url = @page.find(:xpath, "//img[@id='image1']", visible: false)['src']

      return {
          start_date:   date_from,
          end_date:     date_to,
          teaser:       teaser,
          photo_url:    photo_url
      }
    end

    private

    def clean_cookies_alert
      @page.find(:xpath, CLOSE_COOKIE_LINK_XPATH).trigger('click') if @page.has_xpath?(CLOSE_COOKIE_LINK_XPATH)
    end

    def close_modal_window
      @page.find(:xpath, CLOSE_MODAL_LINK_XPATH).trigger('click') if @page.has_xpath?(CLOSE_MODAL_LINK_XPATH)
    end

  end

end
