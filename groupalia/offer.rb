require 'digest'
require_relative '../common/offer'
require_relative '../common/file_utils'
require_relative '../common/local_photo'
include FileUtils

module Groupalia

  class Offer < Common::Offer

    CMS_MODEL = 'offers'
    AFFILIATE = 'groupalia-web'
    MONGO_COL = :groupalia_offers_web

    def initialize(data, env = :development)
      @cms_model      = CMS_MODEL
      @cms_identifier = :landing_url
      @affiliate      = AFFILIATE
      @mongo_col      = MONGO_COL
      @env            = env

      if data.class == BSON::Document
        @data = data
      else
        tags = data[:destination].split(',').collect{|leg| make_tag(leg)}.uniq
        @data = {
            categories:   'escapadas',
            tags:         ['escapada'] + tags,
            title:        "#{data[:title]} desde #{data[:price]}€",
            seo_title:    "Escapada a #{data[:destination]} desde #{data[:price]}€ - Baratrip.es",
            meta_description: "Escápate a #{data[:destination]} con esta oferta de '#{data[:title]}' desde #{data[:price]}€. Entra para conocer más detalles y poder reservar",
            price:        data[:price].to_i,
            destination:  data[:destination],
            landing_url:  data[:url],
            hash_url:     hash_url(data[:url]),
            track_uri:    track_tradedoubler_url(data[:url], 241330, 21437886),
            permalink:    "escapada-#{make_permalink(data[:destination])}"
        }
      end
    end

    def evaluate
      cms_offers = self.class.find_in_cms(@cms_identifier, @data[:landing_url])
      if cms_offers.size > 0
        cms_offer = cms_offers[0]
        cms_offer_data = { edit_url: edit_offer_url(cms_offer['_id']) }
        ['_id', 'title', 'affiliate', 'price', 'posted_at', 'end_date', '_visible'].each{|attr| cms_offer_data[attr] = cms_offer[attr]}
        res = { status: :found, cms_offer_data: cms_offer_data, update_link: update_link(cms_offer['_id']) }
      else
        res = { status: :new, create_link: create_link }
      end
      res
    end

    def add_photo
      photo = Common::LocalPhoto.new("#{@data[:hash_url]}_offer_photo.png")
      photo.import(@data[:photo_url])
      upload_photo_to_cms(photo, "Imagen de #{@data[:destination]}")
    end

    def complete_offer(browser_session)
      new_data = browser_session.get_data_from_final_url(@data[:landing_url])
      @data[:teaser] = "#{new_data[:teaser]} desde #{@data[:price]}€ " + ". Haz clic en la oferta para conocer más detalles."
      @data[:start_date] = new_data[:start_date]
      @data[:end_date] = new_data[:end_date]
      @data[:photo_url] = new_data[:photo_url]
    end

    def take_screenshots(browser_session)
      @data[:main_screenshot] = take_screenshot(browser_session, @data[:landing_url], @data[:hash_url] + "main.png", 'div.contentHead')
      @data[:stars_screenshot] = take_screenshot(browser_session, @data[:landing_url], @data[:hash_url] + "stars.png", 'div.panoramicBullets')
      main_scr = @data[:main_screenshot]
      st_scr = @data[:stars_screenshot]
      title = @data[:title]
      uri = @data[:track_uri]
      desc = "<h2>Detalles de la oferta de escapada a #{@data[:destination]} desde #{@data[:price]}€</h2>"
      desc += "<a href='#{uri}' title='#{title}' target='_blank'><img src='#{main_scr}' alt='#{title}' /></a>"
      desc += "<h3>¿Qué incluye esta oferta de escapada a #{@data[:destination]}?</h3>"
      desc += "<a href='#{uri}' title='#{title}' target='_blank'><img src='#{st_scr}' alt='#{title}' /></a>"
      @data[:description] = desc
    end

  end
end
