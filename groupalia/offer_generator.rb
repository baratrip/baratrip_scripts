require_relative '../common/utils'
require_relative '../common/notification_manager'
require_relative '../groupalia/session'
require_relative '../groupalia/offer'
require_relative '../groupalia/offer_generator_utils'

module Groupalia

  class OfferGenerator
    include Common::Utils
    include OfferGeneratorUtils
    include NotificationManager

    def initialize(env)
      @env = env
      DSL::CMS.set_conn(env)
      DSL::MongoDB.set_conn(env)
      @report_to = report_to(env)
    end

    def get_daily_two_offers
      # Open browser instance and get current public offers
      @browser_session = Session.new
      public_offers = @browser_session.get_public_offers

      # Create Offer instances and group new ones
      offers = init_offers(public_offers, @env)

      # Get more info (i.e. price) for new offers
      enriched_offers = get_more_data_offers(offers)

      # Store all data retrieved in mongo
      stored_offers = store_offers_in_mongo(enriched_offers)

      # Reject offer that we could not get a price
      filtered_offers = reject_no_price(stored_offers)

      # Sort them by price and choose cheapest and most expensive
      sorted_offers = sort_by_price(filtered_offers)

      # Classify between 'new' and 'found'
      evaluated_offers = evaluate_offers(sorted_offers)

      # Cleanup
      @browser_session.close

      # Send email
      deliver_report("Groupalia - Offers from /ofertas-viajes", evaluated_offers)

      evaluated_offers
    end

    private

    def deliver_report(title, offers)
      report = create_report(offers)
      send_report( @report_to, title, report, [] )
    end

    def create_report(list)
      msg = "<p><strong>Offers List (#{list.size})</strong></p>"
      list.each do |item|
        offer = item[0]
        result = item[1]
        msg << "<p>#{offer.data[:title]}: #{offer.data[:price]} <b>(#{result[:status].upcase})</b></p>"
        msg << "<p><span>Tracked url:</span> (#{offer.data[:track_uri]})</p>"
        msg << send("msg_for_#{result[:status]}", result)
        msg << "<p>------------------------------------------------------</p>"
      end
      return msg
    end

    def msg_for_new(result)
      "<p><span>Action:</span> <a href='#{result[:create_link]}'>>> Click here to Create <<</a></p>"
    end

    def msg_for_found(result)
      list = result[:cms_offer_data].map{|k,v| "<li>#{k}:&nbsp;&nbsp;#{v}</li>"}
      "<p>
        <span>Prev Offer Info:</span>
        <ul>
          #{list.join('')}
        </ul>
      </p>
      <p><span>Action:</span> <a href='#{result[:update_link]}'>>> Click here to Update <<</a></p>"
    end

  end
end
