require_relative '../groupalia/offer'

module Groupalia

  module OfferGeneratorUtils

    def init_offers(public_offers, env)
      offers = []
      public_offers.each do |public_offer_data|
        offers << Offer.new(public_offer_data, env)
      end
      offers
    end

    def create_offer(public_offer)
      Offer.new(public_offer)
    end

    def evaluate_offers(list)
      offers_processed = []
      list.each do |offer|
        begin
          res = offer.evaluate
          offers_processed << [offer, res]
        rescue => e
          p e
          RollbarNotifier.error(e, offer.data)
        end
      end
      offers_processed
    end

    # get more data, either from mongo or web
    def get_more_data_offers(offers)
      offers.each do |offer|
        begin
          offer.complete_offer(@browser_session)
        rescue => e
          p e
          RollbarNotifier.error(e, offer.data)
        end
      end
      offers
    end

    def store_offers_in_mongo(offers)
      offers.each do |offer|
        # store a new entry in mongo
        offer.store_in_mongo
      end
    end

  end
end
