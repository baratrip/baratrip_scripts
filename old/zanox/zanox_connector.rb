require 'open-uri'
require 'nokogiri'

class ZanoxConnector


def get_offers
	tbfile = TravelBirdFile.new
	tbfile.unzip
	return tbfile.parse
end


class TravelBirdFile

	FILE_URL = 'http://productdata.zanox.com/exportservice/v1/rest/35006855C1838912815.xml?ticket=11BB92472561713FB244AC66542E92402F454E9568A6414032E8132D68926341&productIndustryId=2&gZipCompress=yes'

	FILE_NAME = 'zanox_travelbird'
	
	def initialize	
		@gz_filename = Time.now.strftime("%Y_%m_%d_%H_%M_%S") + '_' + FILE_NAME + '.xml.gz'

		File.open("tmp/#{@gz_filename}", "wb") do |saved_file|
		  open(FILE_URL, "rb") do |read_file|
			saved_file.write(read_file.read)
		  end
		end
	end
	
	def gz_location
		return 'tmp/' + @gz_filename
	end
	
	def gz_filename	
		return @gz_filename
	end
	
	def set_filename
		@filename = self.gz_filename.gsub('.gz','')
	end
	
	def filename	
		return @filename
	end
	
	def file_location	
		return 'tmp/' + @filename
	end
	
	def unzip
		self.set_filename
		Zlib::GzipReader.open(self.gz_location) do |gz|
	  		File.open(self.file_location, "w") do |g|
				IO.copy_stream(gz, g)
	  		end
		end
	end
	
	def parse
		resp = nil
		File.open(self.file_location) do |f|
		  doc = Nokogiri::XML(f)
		  items = doc.xpath('//product')
		  main_attrs = ['number', 'deepLink', 'price', 'oldPrice', 'name', 'description' , 'longDescription', 'largeImage', 'program']

		  resp = items.collect{ |item| parse_node_attrs(main_attrs, item) }
    	end
    	
    	return resp
	end
	
	private
	def parse_node_attrs(attrs_list, node)
		attrs = {}
		attrs_list.each do |attr|
		  attr_node = node.at(attr)
		  attrs[attr] = attr_node.inner_html if attr_node
		end
		return attrs
  	end

end




end
