require_relative './zanox_connector'
require_relative './baratrip_connector'

class ZanoxImporter


def initialize(env)
	@govi_conn = BaratripConnector.new(env)
	@zanox_conn = ZanoxConnector.new
end

def get_new_offers
	active_offers = @zanox_conn.get_offers
	
	active_offers.each do |offer|
		offer_id = offer['number']
		if offer_exists?(offer_id)
			update_offer(offer_id, offer)
		else
			offer_content = create_offer(offer)
			raise "Not stored properly #{offer_content.title}" if offer_content.created_at.nil?
		end		 
	end
end

private

def offer_exists?(offer_id)
	offer = @govi_conn.find_content('offers', {'affiliate_offer_id': offer_id})		
	return !offer.empty? 
end


def update_offer(offer_id, offer)
	new_offer =  offer_struct(offer)	
	offer = @govi_conn.find_content('offers', {'affiliate_offer_id': offer_id})		
	@govi_conn.update_content('offers', offer[0].attributes['_id'], new_offer)
end
	

def create_offer(offer)
	offer =  offer_struct(offer)	
	p offer
	@govi_conn.create_content('offers', offer)    
end


def offer_struct(offer)
	{
		title: offer['name'],
		teaser: offer['description'],
		description: offer['longDescription'],
		affiliate: 'zanox',
		affiliate_program: offer['program'],
		affiliate_offer_id: offer['number'],		
		track_uri: offer['deepLink'],	
		price: offer['price'],
		old_price: offer['oldPrice'],
	}
end
end


ZanoxImporter.new(:development).get_new_offers

