require_relative '../common/baratrip_cms'

module TradeDoubler

  class Promocode
    include BaratripCMS

    CMS_MODEL = 'promocodes'

    def initialize(voucher)
      @data = {
          code: voucher['code'],
          code_type:  voucher['voucherTypeId'],
          title: voucher['title'],
          teaser: voucher['shortDescription'],
          description: voucher['description'],
          affiliate: 'tradedoubler',
          affiliate_voucher_id: voucher['id'],
          landing_url: voucher['landingUrl'],
          track_uri: voucher['defaultTrackUri'],
          exclusive: voucher['exclusive'],
          start_date: Time.at(voucher['startDate'].to_i/1000),
          end_date: Time.at(voucher['endDate'].to_i/1000)
      }
    end

    def []
      @data
    end

    def data
      @data
    end

    def [](attr)
      @data[attr]
    end

    def update_cms_entry
      cms_obj = DSL::CMS.find(CMS_MODEL, { "affiliate_voucher_id": @data[:affiliate_voucher_id] })
      DSL::CMS.update('promocodes', cms_obj.attributes['_id'], @data)
      @data[:cms] = edit_promocode_url(cms_obj.attributes['_id'])
    end

    def already_in_cms?
      DSL::CMS.already_in_cms?(CMS_MODEL, 'affiliate_voucher_id', @data[:affiliate_voucher_id])
    end

    def store_in_cms
      id = DSL::CMS.store_in_cms(CMS_MODEL, @data)
      @data[:cms] = edit_promocode_url(id)
    end

  end

end
