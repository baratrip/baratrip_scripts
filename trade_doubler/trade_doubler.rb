require_relative '../common/dsl/dsl'
require_relative '../common/notification_manager'
require_relative '../trade_doubler/feed_connector'
require_relative '../trade_doubler/promocode'

require 'csv'

module TradeDoubler

  class Script
    include NotificationManager

    def initialize(env)
      @env = env
      @feed = FeedConnector.new
      DSL::CMS.set_conn(env)
      DSL::MongoDB.set_conn(env)
      @report_to = report_to(env)
    end

    def env
      @env
    end

    def get_active_vouchers
      @report_title = "TradeDoubler - new promocodes"
      old_vouchers = []
      new_vouchers = []

      active_vouchers = @feed.get_active_vouchers

      active_vouchers.each do |voucher|
        # create promocode object
        promocode = Promocode.new(voucher)
        in_cms = promocode.already_in_cms?

        if in_cms
          promocode.update_cms_entry
          old_vouchers << promocode
        else
          promocode.store_in_cms
          new_vouchers << promocode
        end
      end

      deliver_report(new_vouchers, old_vouchers)

      { new: new_vouchers, updates: old_vouchers }

    end


    private

    def deliver_report(created, updated)
      create_report(created, updated)
      send_report( @report_to, @report_title, @report, [] )
    end

    def create_report(created, updated)
      @report = "<h3>Created</h3>"
      created.each do |offer|
        data = offer.data
        @report << "<p><strong>#{data[:title]}</strong></p>"
        @report << "<p>Edit in CMS:  <a href='#{data[:cms]}' target='_blank'>click here</a></p>"
      end
      @report += "<h3>Updated</h3>"
      updated.each do |offer|
        data = offer.data
        @report << "<p><strong>#{data[:title]}</strong></p>"
        @report << "<p>Edit in CMS:  <a href='#{data[:cms]}' target='_blank'>click here</a></p>"
      end
    end

  end

end

