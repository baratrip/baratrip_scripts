#!/bin/bash
# Get trade_doubler current vouchers
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching trade_doubler_importer"
ruby trade_doubler_launcher.rb development "get_active_vouchers"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished trade_doubler_importer. Elapsed time $(($ENDTIME - $STARTTIME))"
