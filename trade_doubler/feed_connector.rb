require 'date'
require 'net/http'
require 'uri'
require 'json'

module TradeDoubler

	class FeedConnector
		API_URL = 'http://api.tradedoubler.com/1.0/'
		VOUCHERS_TOKEN = '72B672F7F18CA3EA2D5505406BD5D20B540BCA9F'

		PRODUCT_FEEDS = {
			groupalia: {
				feed_no: '14565755212443327',
				format: '14565758222443327'
			},
			letsbonus: {
				feed_no: '14601978682443327',
				format: '14565758222443327'
			}
		}


		def get_active_vouchers(limit_date = nil)
			limit_date = limit_date ? Date.parse(limit_date) : (Date.today + 1)
			epoch_time = limit_date.to_time.to_i * 1000
			query = "minEndDate=#{epoch_time}"
			return send_voucher_request('vouchers', 'json', query)
		end

		def get_feed(feed_name)
			feed = PRODUCT_FEEDS[feed_name]
			filename = "tmp/#{feed_name}_feed.zip"
			uri = URI("http://pf.tradedoubler.com/export/export?myFeed=#{feed[:feed_no]}&myFormat=#{feed[:format]}")
			File.write(filename, Net::HTTP.get(uri))
			data = FileUtils.unzipFile(filename)
			return data
		end

		private

		def send_voucher_request(resource, format, query)
			url = API_URL + resource + '.' + format + ';' + query + '?token='+VOUCHERS_TOKEN
			uri = URI(url)
			return JSON.parse(Net::HTTP.get(uri))
		end

	end
end
