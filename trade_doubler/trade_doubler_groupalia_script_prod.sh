#!/bin/bash
# Get trade_doubler current groupalia offers
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching trade_doubler_importer in production mode"
ruby /home/apps/baratrip_scripts/trade_doubler/trade_doubler_importer.rb production "get_new_groupalia_offers"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished trade_doubler_importer. Elapsed time $(($ENDTIME - $STARTTIME))"
