#!/bin/sh
DIR=`date +%y%m%d`
DEST=/home/apps/db_backups/$DIR
mkdir $DEST
mongodump -h localhost -o $DEST
aws s3 cp $DEST s3://baratrip/db-backup/scripts/$DIR --recursive
rm -r $DEST
