require 'date'
require 'sinatra'
require_relative '../booking/booking'
require_relative '../skyscanner/skyscanner'
require_relative '../groupalia/groupalia'
require_relative '../logitravel/logitravel'

  get '/ping' do
    'Put this in your pipe & smoke it!'
  end

  get '/offer/:id/create' do
    offer_type = params['offer_type']
    handler = create_handler(offer_type)
    cms_url = handler.create_cms_offer(params['id'])
    redirect to(cms_url)
  end

  get '/offer/:id/update/:cms_id' do
    offer_type = params['offer_type']
    handler = create_handler(offer_type)
    cms_url = handler.update_cms_offer(params['id'], params['cms_id'])
    redirect to(cms_url)
  end

  get '/offer/:id/add_hotel' do
    destination = params['destination']
    checkin = Date.parse(params['checkin'])
    checkout = Date.parse(params['checkout'])
    @title = "#{destination} . From: #{checkin} to #{checkout}"
    #@resp = { local: "tmp/test.png", s3: "https://baratrip.s3-eu-west-1.amazonaws.com/screenshots/test" }
    @resp = Booking::Session.new.get_cheapest_hotel(destination, checkin, checkout)
    #@resp << Booking::Session.new.get_cheapest_recommended_hotel(destination, checkin, checkout)
    p @resp
    erb :hotel
  end

  def create_handler(offer_type)
    env = Sinatra::Application.environment
    case offer_type
    when 'skyscanner'
      Skyscanner::OfferHandler.new(env)
    when 'groupalia-web'
      Groupalia::OfferHandler.new(env)
    when 'logitravel'
      Logitravel::OfferHandler.new(env)
    end
  end
  # http://localhost:4567/offer/111111/add_hotel?destination=malaga&checkin=2016-10-10&checkout=2016-10-12
