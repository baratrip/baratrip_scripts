require_relative '../../groupalia/offer'


RSpec.describe Groupalia, "offer" do
  let(:offer_data) {
    {destination: 'Albacete',
    title: 'Nochevieja en Albacete',
    url: 'http://baratrip.es'}
  }

  context "#new" do
    it "is properly initialized" do
      offer = Groupalia::Offer.new(offer_data)
      expect(offer[:landing_url]).to eql('http://baratrip.es')
      expect(offer[:hash_url]).to eql('4423f18db27ff090918c6f03071be75d01d599b27f3118b151867dac6f0575f0')
      expect(offer[:track_uri]).to eql('http://clk.tradedoubler.com/click?p(241330)a(2443327)g(21437886)url(http://baratrip.es)')
      expect(offer.affiliate).to eql('groupalia-web')
      expect(offer.cms_model).to eql('offers')
      expect(offer.mongo_col).to eql(:groupalia_offers_web)
    end
  end

  context "#map_data_to_cms" do

    it "important fields are properly mapped" do
      offer = Groupalia::Offer.new(offer_data)
      cms_data = offer.map_data_to_cms
      expect(cms_data[:landing_url]).to eql('http://baratrip.es')
      expect(cms_data[:track_uri]).to eql('http://clk.tradedoubler.com/click?p(241330)a(2443327)g(21437886)url(http://baratrip.es)')
      expect(cms_data[:affiliate]).to eql('groupalia-web')
    end

  end

  context "#take_screenshots" do

    it "takes screenshots and adds them to description" do
      browser_session = instance_double("Groupalia::Session")
      allow(browser_session).to receive(:take_screenshot).with('http://baratrip.es', '4423f18db27ff090918c6f03071be75d01d599b27f3118b151867dac6f0575f0main.png', 'div.contentHead', false).exactly(1).times.and_return('/s3/local1')
      allow(browser_session).to receive(:take_screenshot).with('http://baratrip.es', '4423f18db27ff090918c6f03071be75d01d599b27f3118b151867dac6f0575f0stars.png', 'div.panoramicBullets', false).exactly(1).times.and_return('/s3/local2')
      offer = Groupalia::Offer.new(offer_data)
      allow(offer).to receive(:upload_screenshot).with('/tmp/local1', '4423f18db27ff090918c6f03071be75d01d599b27f3118b151867dac6f0575f0main.png').exactly(1).times
      .and_return('/s3/local1')
      allow(offer).to receive(:upload_screenshot).with('/tmp/local2', '4423f18db27ff090918c6f03071be75d01d599b27f3118b151867dac6f0575f0stars.png').exactly(1).times
      .and_return('/s3/local2')
      offer.take_screenshots(browser_session)
      expect(offer.data[:description]).to eql("<h2>Detalles de la oferta de escapada a Albacete desde 0€</h2><a href='http://clk.tradedoubler.com/click?p(241330)a(2443327)g(21437886)url(http://baratrip.es)' title='Nochevieja en Albacete desde €' target='_blank'><img src='/s3/local1' alt='Nochevieja en Albacete desde €' /></a><h3>¿Qué incluye esta oferta de escapada a Albacete?</h3><a href='http://clk.tradedoubler.com/click?p(241330)a(2443327)g(21437886)url(http://baratrip.es)' title='Nochevieja en Albacete desde €' target='_blank'><img src='/s3/local2' alt='Nochevieja en Albacete desde €' /></a>")
    end

  end

  context "#evaluate" do

      it "evaluates as new offer in cms if none found" do
        allow(DSL::CMS).to receive(:find).exactly(1).times.and_return([])
        offer = Groupalia::Offer.new(offer_data)

        # Important. Storing offer in mongo in order to have the id for the update_link
        mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
        allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
        offer.store_in_mongo

        res = offer.evaluate
        expect(res[:status]).to eql(:new)
        expect(res[:create_link]).to eql("http://localhost:4567/offer/1/create?offer_type=groupalia-web")
      end

      it "evaluates as found offer" do
        allow(DSL::CMS).to receive(:find).exactly(1).times.and_return([{'title'=> 'previous offer', '_id'=> 1}])
        offer = Groupalia::Offer.new(offer_data)

        # Important. Storing offer in mongo in order to have the id for the update_link
        mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
        allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
        offer.store_in_mongo

        res = offer.evaluate
        expect(res[:status]).to eql(:found)
        expect(res[:update_link]).to eql("http://localhost:4567/offer/1/update/1?offer_type=groupalia-web")
      end
    end
end
