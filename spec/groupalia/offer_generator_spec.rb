require_relative '../../groupalia/offer_generator'

RSpec.describe Groupalia, "#offer_generator" do

  context "with no offers" do

    it "returns empty array" do

      # Avoid connecting to CMS
      allow(DSL::CMS).to receive(:set_conn).exactly(1).times.and_return(true)
      allow(DSL::MongoDB).to receive(:set_conn).exactly(1).times.and_return(true)

      # Mock browser session
      session = instance_double("Groupalia::Session")
      allow(Groupalia::Session).to receive(:new).exactly(1).times.and_return(session)
      allow(session).to receive(:close).exactly(1).times.and_return(true)

      # No offers found by Browser
      allow(session).to receive(:get_public_offers).and_return([])

      groupalia = Groupalia::OfferGenerator.new(:test)
      allow(groupalia).to receive(:deliver_report).exactly(1).times.and_return(true)
      expect(groupalia.get_daily_two_offers).to eq([])
    end

  end

  context "with two offers" do

    it "returns both ordered by price ASC" do

      # Avoid connecting to CMS
      allow(DSL::CMS).to receive(:set_conn).exactly(1).times.and_return(true)
      allow(DSL::MongoDB).to receive(:set_conn).exactly(1).times.and_return(true)

      # Mock browser session
      session = instance_double("Groupalia::Session")
      allow(Groupalia::Session).to receive(:new).exactly(1).times.and_return(session)
      allow(session).to receive(:close).exactly(1).times.and_return(true)

      # Offers found by Browser
      allow(session).to receive(:get_public_offers).and_return(['offer1', 'offer2'])

      # Instances of Groupalia::Offer for such offers
      offer1 = instance_double("Groupalia::Offer", data: { landing_url: 'offer1', price: 100 }, evaluate: { status: :new, create_link: '' }, complete_offer: true, already_in_mongo?: false, store_in_mongo: true)
      offer2 = instance_double("Groupalia::Offer", data: { landing_url: 'offer2', price: 1 }, evaluate: { status: :new, create_link: '' }, complete_offer: true, already_in_mongo?: false, store_in_mongo: true)

      # expect(Groupalia::Offer).to receive(:new).exactly(2).times.and_return(offer1, offer2)

      groupalia = Groupalia::OfferGenerator.new(:test)
      expect(groupalia).to receive(:init_offers).and_return([offer1,offer2])
      expect(groupalia).to receive(:deliver_report).and_return(true)
      expect(groupalia.get_daily_two_offers).to eq([[offer2, { status: :new, create_link: '' }], [offer1, { status: :new, create_link: '' }]])
    end

  end
end


  context "with three offers. one is published already" do

    it "returns the three ordered by price ASC" do

      # Avoid connecting to CMS
      allow(DSL::CMS).to receive(:set_conn).exactly(1).times.and_return(true)
      allow(DSL::MongoDB).to receive(:set_conn).exactly(1).times.and_return(true)

      # Mock browser session
      session = instance_double("Groupalia::Session")
      allow(Groupalia::Session).to receive(:new).exactly(1).times.and_return(session)
      allow(session).to receive(:close).exactly(1).times.and_return(true)

      # Offers found by Browser
      expect(session).to receive(:get_public_offers).and_return(['offer1', 'offer2', 'offer3'])

      # Instances of Groupalia::Offer for such offers
      offer1 = instance_double("Groupalia::Offer", data: { landing_url: 'offer1', price: 100 }, complete_offer: true, already_in_mongo?: false, evaluate: { status: :new, create_link: '' }, store_in_mongo: true)
      offer2 = instance_double("Groupalia::Offer", data: { landing_url: 'offer2', price: 1 }, complete_offer: true, already_in_mongo?: true,  evaluate: { status: :found, update_link: '' }, enrich_from_mongo: true, store_in_mongo: true)
      offer3 = instance_double("Groupalia::Offer", data: { landing_url: 'offer3', price: 1 }, complete_offer: true, already_in_mongo?: false, evaluate: { status: :new, create_link: '' }, store_in_mongo: true)

      groupalia = Groupalia::OfferGenerator.new(:test)
      expect(groupalia).to receive(:init_offers).and_return([offer1,offer2,offer3])
      expect(groupalia).to receive(:deliver_report).and_return(true)
      expect(groupalia.get_daily_two_offers).to eq([[offer2, { status: :found, update_link: '' }], [offer3, { status: :new, create_link: '' }], [offer1, { status: :new, create_link: '' }]])
    end

  end
