require_relative '../../groupalia/offer_handler'

RSpec.describe Groupalia, "OfferHandler" do

  context "#create_cms_offer" do

    it "retrieves offer from mongo, take screenshots and send to cms" do
      # Avoid connecting to CMS
      allow(DSL::CMS).to receive(:set_conn).exactly(1).times.and_return(true)
      allow(DSL::MongoDB).to receive(:set_conn).exactly(1).times.and_return(true)

      # Mock offer
      offer = instance_double("Groupalia::Offer", data: { landing_url: 'offer1', price: 100 }, take_screenshots: true, store_in_cms: 'https://google.es', add_photo: true)
      allow(Groupalia::Offer).to receive(:restore).with(:groupalia_offers_web, '_id', BSON::ObjectId('507f191e810c19729de860ea')).exactly(1).times.and_return(offer)

      # Mock browser session
      session = instance_double("Groupalia::Session")
      allow(Groupalia::Session).to receive(:new).exactly(1).times.and_return(session)
      allow(session).to receive(:close).exactly(1).times.and_return(true)

      handler = Groupalia::OfferHandler.new(:test)
      url = handler.create_cms_offer('507f191e810c19729de860ea')
      expect(url).to eql('https://google.es')
    end

  end

  context "#update_cms_offer" do

    it "retrieves offer from mongo, take screenshots and send to cms" do
      # Avoid connecting to CMS
      allow(DSL::CMS).to receive(:set_conn).exactly(1).times.and_return(true)
      allow(DSL::MongoDB).to receive(:set_conn).exactly(1).times.and_return(true)

      # Mock offer
      offer = instance_double("Groupalia::Offer", data: { landing_url: 'offer1', price: 100 }, take_screenshots: true, update_cms_offer: 'https://google.es')
      allow(Groupalia::Offer).to receive(:restore).with(:groupalia_offers_web, '_id', BSON::ObjectId('507f191e810c19729de860ea')).exactly(1).times.and_return(offer)

      # Mock browser session
      session = instance_double("Groupalia::Session")
      allow(Groupalia::Session).to receive(:new).exactly(1).times.and_return(session)
      allow(session).to receive(:close).exactly(1).times.and_return(true)

      handler = Groupalia::OfferHandler.new(:test)
      url = handler.update_cms_offer('507f191e810c19729de860ea', 1)
      expect(url).to eql('https://google.es')
    end

  end

end
