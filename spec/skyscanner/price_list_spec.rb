require_relative '../../skyscanner/models/price_list'
require_relative '../../skyscanner/models/price_item'
require_relative '../../skyscanner/models/place'

include Skyscanner


RSpec.describe Skyscanner::PriceList, "#new" do

  it "is properly initialized" do
    data = [
      {"QuoteId"=>1, "MinPrice"=>305.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>73268, "DestinationId"=>50290, "DepartureDate"=>"2016-05-25T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>50290, "DestinationId"=>73268, "DepartureDate"=>"2016-06-09T00:00:00"}, "QuoteDateTime"=>"2016-05-19T08:35:00"},
      {"QuoteId"=>2, "MinPrice"=>18.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>88915, "DestinationId"=>86622, "DepartureDate"=>"2016-12-02T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>86622, "DestinationId"=>88915, "DepartureDate"=>"2016-12-10T00:00:00"}, "QuoteDateTime"=>"2016-05-12T22:24:00"},
      {"QuoteId"=>3, "MinPrice"=>384.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42414, "DestinationId"=>42795, "DepartureDate"=>"2016-06-08T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42795, "DestinationId"=>42414, "DepartureDate"=>"2016-06-21T00:00:00"}, "QuoteDateTime"=>"2016-05-21T10:29:00"}
    ]
    place = instance_double("Place", id: 3, name: 'Almeria', code: 'CU')
    allow(Place).to receive(:get_data).and_return(place)
    pricelist = PriceList.new('ES', 'anywhere', data)
    expect(pricelist.from).to eql('ES')
    expect(pricelist.to).to eql('anywhere')
    cheapest = pricelist.list.first
    expect(cheapest.price).to eql(18.0)
    expect(cheapest.destination.name).to eql('Almeria')
  end

  it "ignores items without price" do
    countries =  []
    pricelist = PriceList.new('ES', 'anywhere', countries)
    expect(pricelist.from).to eql('ES')
    expect(pricelist.to).to eql('anywhere')
    expect(pricelist.size).to eql(0)
  end

end

RSpec.describe Skyscanner::PriceList, "#get_cheapest_list_always" do

  context "empty db" do
    it "returns same list" do
      data = [{"QuoteId"=>1, "MinPrice"=>305.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>73268, "DestinationId"=>50290, "DepartureDate"=>"2016-05-25T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>50290, "DestinationId"=>73268, "DepartureDate"=>"2016-06-09T00:00:00"}, "QuoteDateTime"=>"2016-05-19T08:35:00"}, {"QuoteId"=>2, "MinPrice"=>18.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>88915, "DestinationId"=>86622, "DepartureDate"=>"2016-12-02T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>86622, "DestinationId"=>88915, "DepartureDate"=>"2016-12-10T00:00:00"}, "QuoteDateTime"=>"2016-05-12T22:24:00"}, {"QuoteId"=>3, "MinPrice"=>384.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42414, "DestinationId"=>42795, "DepartureDate"=>"2016-06-08T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42795, "DestinationId"=>42414, "DepartureDate"=>"2016-06-21T00:00:00"}, "QuoteDateTime"=>"2016-05-21T10:29:00"}]
      place = instance_double("Place", id: 3, name: 'Almeria', code: 'CU')
      allow(Place).to receive(:get_data).and_return(place)
      pricelist = PriceList.new('ES', 'anywhere', data)
      cheapest_list = pricelist.get_cheapest_list_always
      expect(cheapest_list.size).to eql(pricelist.size)
    end
  end

  context "non-empty db" do
    it "returns empty list if none are the cheapest for their route" do
      data = [{"QuoteId"=>1, "MinPrice"=>305.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>73268, "DestinationId"=>50290, "DepartureDate"=>"2016-05-25T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>50290, "DestinationId"=>73268, "DepartureDate"=>"2016-06-09T00:00:00"}, "QuoteDateTime"=>"2016-05-19T08:35:00"}, {"QuoteId"=>2, "MinPrice"=>18.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>88915, "DestinationId"=>86622, "DepartureDate"=>"2016-12-02T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>86622, "DestinationId"=>88915, "DepartureDate"=>"2016-12-10T00:00:00"}, "QuoteDateTime"=>"2016-05-12T22:24:00"}, {"QuoteId"=>3, "MinPrice"=>384.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42414, "DestinationId"=>42795, "DepartureDate"=>"2016-06-08T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42795, "DestinationId"=>42414, "DepartureDate"=>"2016-06-21T00:00:00"}, "QuoteDateTime"=>"2016-05-21T10:29:00"}]
      place = instance_double("Place", id: 3, name: 'Almeria', code: 'CU')
      allow(Place).to receive(:get_data).and_return(place)
      pricelist = PriceList.new('ES', 'anywhere', data)
      pricelist.store

      pricelist2 = PriceList.new('ES', 'anywhere', data)
      pricelist2.store
      cheapest_list = pricelist2.get_cheapest_list_always
      expect(cheapest_list.size).to eql(0)
    end

    it "returns items that are the cheapest for their route" do
      data = [{"QuoteId"=>1, "MinPrice"=>305.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>73268, "DestinationId"=>50290, "DepartureDate"=>"2016-05-25T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1760], "OriginId"=>50290, "DestinationId"=>73268, "DepartureDate"=>"2016-06-09T00:00:00"}, "QuoteDateTime"=>"2016-05-19T08:35:00"}, {"QuoteId"=>2, "MinPrice"=>18.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>88915, "DestinationId"=>86622, "DepartureDate"=>"2016-12-02T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>86622, "DestinationId"=>88915, "DepartureDate"=>"2016-12-10T00:00:00"}, "QuoteDateTime"=>"2016-05-12T22:24:00"}, {"QuoteId"=>3, "MinPrice"=>384.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42414, "DestinationId"=>42795, "DepartureDate"=>"2016-06-08T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1414], "OriginId"=>42795, "DestinationId"=>42414, "DepartureDate"=>"2016-06-21T00:00:00"}, "QuoteDateTime"=>"2016-05-21T10:29:00"}]
      place = instance_double("Place", id: 3, name: 'Almeria', code: 'CU')
      allow(Place).to receive(:get_data).and_return(place)
      pricelist = PriceList.new('ES', 'anywhere', data)
      pricelist.store

      data2 = [{"QuoteId"=>2, "MinPrice"=>15.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>88915, "DestinationId"=>86622, "DepartureDate"=>"2016-12-02T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1717], "OriginId"=>86622, "DestinationId"=>88915, "DepartureDate"=>"2016-12-10T00:00:00"}, "QuoteDateTime"=>"2016-05-12T22:24:00"}]
      pricelist2 = PriceList.new('ES', 'anywhere', data2)
      pricelist2.store
      cheapest_list = pricelist2.get_cheapest_list_always
      expect(cheapest_list.size).to eql(1)
      expect(cheapest_list[0].class).to eql(PriceItem)
    end
  end

end
