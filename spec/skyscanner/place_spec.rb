require_relative '../../skyscanner/models/place'

include Skyscanner


RSpec.describe Skyscanner::Place, "#get_name" do

  it "raises RuntimeError if does not exist" do
    expect { Place.get_data(42414) }.to raise_error(RuntimeError)
  end

  it "returns value if does exist" do
    Place.store({"PlaceId"=>837, "Name"=>"Cuenca", "Type"=>"Country", "SkyscannerCode"=>"AE"})
    place = Place.get_data(837)
    expect(place.name).to eq('Cuenca')
  end

end
