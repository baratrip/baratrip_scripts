require_relative '../../skyscanner/models/price_item'
require_relative '../../skyscanner/models/place'

include Skyscanner


describe "PriceItem methods"  do

  let(:price_item_data) { { "QuoteId"=>233, "MinPrice"=>119.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1001], "OriginId"=>42414, "DestinationId"=>79576, "DepartureDate"=>"2016-11-17T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1914], "OriginId"=>79576, "DestinationId"=>42414, "DepartureDate"=>"2016-12-02T00:00:00"}, "QuoteDateTime"=>"2016-05-10T19:46:00" } }
  before(:example) do
    stub_places_db
  end

  describe "#new" do

    it "is properly initialized" do
      item = PriceItem.new(price_item_data)
      expect(item.price).to eql(119.0)
      expect(item.origin.name).to eql("Cuenca")
      expect(item.origin.code).to eql("CU")
      expect(item.destination.name).to eql("Albacete")
      expect(item.destination.code).to eql("AL")
    end

  end

  describe "#store" do

    it "is stored properly" do
      time = Date.today
      # Stub storing in mongoDb
      mongo_object = double("mongo_object")
      allow(mongo_object).to receive(:inserted_id) { 1 }
      expect(DSL::MongoDB).to receive(:store).with("skyscanner_quotes", { :price=>119.0,
                                                                          :direct=>false,
                                                                          :created_at=>time,
                                                                          :updated_at=>"2016-05-10T19:46:00",
                                                                          :origin_id=>1,
                                                                          :origin_name=>"Cuenca",
                                                                          :destination_id=> 2,
                                                                          :destination_name=>"Albacete",
                                                                          :out_departure_date=>"2016-11-17T00:00:00",
                                                                          :in_departure_date=>"2016-12-02T00:00:00"
                                                                      }
                              ).and_return(mongo_object)


      item = PriceItem.new(price_item_data)
      item.store
    end

  end

  describe "#is_first_item_for_route?" do

    context "db is empty" do
      it "returns true" do
        item = PriceItem.new(price_item_data)
        expect(item.is_first_item_for_route?).to eq(true)
      end
    end

    context "db has this route " do
      it "returns false" do
        stub_places_db
        item = PriceItem.new(price_item_data)
        item.store

        item2 = PriceItem.new(price_item_data)
        item2.store
        expect(item2.is_first_item_for_route?).to eq(false)
      end
    end

  end

  describe "#is_cheapest?" do

    context "db is empty" do
      it "returns true" do
        stub_places_db
        item = PriceItem.new(price_item_data)
        expect(item.is_cheapest?).to eq(true)
      end
    end

    context "db not empty" do

      it "returns false if same price" do
        stub_places_db
        item = PriceItem.new(price_item_data)
        item.store
        expect(item.is_cheapest?).to eq(true)

        item2 = PriceItem.new(price_item_data)
        item2.store
        expect(item2.is_cheapest?).to eq(false)
      end

      it "returns true if lower price" do
        stub_places_db
        item = PriceItem.new(price_item_data)
        item.store

        price_item_data["MinPrice"] = 10
        item2 = PriceItem.new(price_item_data)
        item2.store

        expect(item.is_cheapest?).to eq(false)
        expect(item2.is_cheapest?).to eq(true)
      end
    end

  end

  describe "#is_cheapest_last_month?" do

    context "db is empty" do
      it "returns true" do
        stub_places_db
        item = PriceItem.new(price_item_data)
        expect(item.is_cheapest_last_month?).to eq(true)
      end
    end

    context "db not empty" do

      it "returns false if same price within last 30 days" do

        item = PriceItem.new(price_item_data)
        item.store
        expect(item.is_cheapest_last_month?).to eq(true)

        item2 = PriceItem.new(price_item_data)
        item2.store
        expect(item2.is_cheapest_last_month?).to eq(false)
      end

      it "returns true if same price but older than 30 days" do

        item = PriceItem.new(price_item_data)
        item.created_at = Date.today - 31
        item.store

        price_item_data["MinPrice"] = 10
        item2 = PriceItem.new(price_item_data)
        item2.store

        expect(item2.is_cheapest_last_month?).to eq(true)
      end
    end
  end
end

def stub_places_db
  place = instance_double("Place", id: 1, name: 'Cuenca', code: 'CU')
  place2 = instance_double("Place", id: 2, name: 'Albacete', code: 'AL')
  allow(Place).to receive(:get_data).with(42414).and_return(place)
  allow(Place).to receive(:get_data).with(79576).and_return(place2)
end
