require_relative '../../skyscanner/offer_generator_utils'
require_relative '../../skyscanner/api_client'
require_relative '../../skyscanner/offer'
require_relative '../../skyscanner/models/place'
require_relative '../../skyscanner/models/price_list'
require_relative '../../skyscanner/models/price_item'
include Skyscanner
include Skyscanner::OfferGeneratorUtils

describe "OfferGeneratorUtils methods"  do

  let(:api_result) {
    {
      "Places" => [{ "PlaceId"=>837, "Name"=>"Cuenca", "Type"=>"Country", "SkyscannerCode"=>"AE" }],
      "Quotes" => [{ "QuoteId"=>233, "MinPrice"=>119.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1001], "OriginId"=>42414, "DestinationId"=>79576, "DepartureDate"=>"2016-11-17T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1914], "OriginId"=>79576, "DestinationId"=>42414, "DepartureDate"=>"2016-12-02T00:00:00"}, "QuoteDateTime"=>"2016-05-10T19:46:00" }]
    }
  }

  before(:example) do
    api_client = instance_double("ApiClient", browse_quotes: api_result)
    allow(ApiClient).to receive(:new).exactly(1).times.and_return(api_client)
    allow(Place).to receive(:store).exactly(1).times.and_return(true)
    place = instance_double("Place", id: 1, name: 'Cuenca', code: 'CU', city: 'Cuenca', country: 'Spain')
    place2 = instance_double("Place", id: 3, name: 'Río de Janeiro', code: 'RI', city: 'Río de Janeiro', country: 'Brazil')
    allow(Place).to receive(:get_data).with(42414).exactly(1).times.and_return(place)
    allow(Place).to receive(:get_data).with(79576).exactly(1).times.and_return(place2)
  end

  describe "get_new_price_list"  do
      it "returns a Pricelist" do
        res = get_new_price_list('Spain', 'anywhere')
        expect(res.class).to eql(PriceList)
        expect(res.list.size).to eql(1)
      end
  end

  describe "evaluate_offers"  do
      it "is properly initialized" do
        price_list = get_new_price_list('Spain', 'anywhere')
        status = :found
        data = ["http://baratrip.es/locomotive/nely-moss-8000/content_types/offers/entries/1/edit"]
        offer = instance_double("Offer", id: 1, store_in_mongo: true, evaluate: { status: status, update_link: data })
        allow(Offer).to receive(:new).exactly(1).times.and_return(offer)

        res = evaluate_offers(:test, price_list.list)
        expect(res.size).to eql(1)
        expect(res[0]).to eql([offer, {:status => status, :update_link => data }])
      end
  end

end
