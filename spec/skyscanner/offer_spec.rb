require_relative '../../skyscanner/offer'
require_relative '../../skyscanner/models/price_item'
require_relative '../../skyscanner/models/place'

include Skyscanner

describe "Offer methods"  do

  let(:price_item_data) { { "QuoteId"=>233, "MinPrice"=>119.0, "Direct"=>false, "OutboundLeg"=>{"CarrierIds"=>[1001], "OriginId"=>42414, "DestinationId"=>79576, "DepartureDate"=>"2016-11-17T00:00:00"}, "InboundLeg"=>{"CarrierIds"=>[1914], "OriginId"=>79576, "DestinationId"=>42414, "DepartureDate"=>"2016-12-02T00:00:00"}, "QuoteDateTime"=>"2016-05-10T19:46:00" } }
  before(:example) do
    stub_places
  end

  describe "#new" do
    it "is properly initialized" do
      item = PriceItem.new(price_item_data)
      offer = Offer.new(item)
      expect(offer.price_item).to eql(item)
      offer_data = offer.data
      expect(offer_data[:price]).to eql(119.0)
      expect(offer_data[:title]).to eql("Vuelo de Cuenca a Río de Janeiro ida y vuelta desde 119.0€")
      expect(offer_data[:seo_title]).to eql("Vuelo de Cuenca a Río de Janeiro desde 119.0€ - Baratrip.es")
      expect(offer_data[:meta_description]).to eql("Conoce Brasil gracias a este vuelo de Cuenca a Río de Janeiro desde 119.0€")
      expect(offer_data[:teaser]).to eql("Viaja a Río de Janeiro con esta oferta de vuelo directo desde Cuenca por tan solo 119.0€ ida y vuelta. Entra para conocer las fechas y más detalles del vuelo")
      expect(offer_data[:description]).to eql("<h2>Detalles de la oferta de vuelo de Cuenca a Río de Janeiro desde 119.0€</h2><ul><li>Vuelo de ida de Cuenca a Río de Janeiro el 2016-11-17</li><li>Vuelo de regreso de Río de Janeiro a Cuenca el 2016-12-02</li><li>Vuelo con escala</li></ul>")
      expect(offer_data[:tags_groups]).to eql(
      {
        :offer => [["cuenca", "rio de janeiro", "brasil"], ["cuenca", "rio de janeiro"]],
        :post => [["cuenca", "rio de janeiro"], ["cuenca", "brasil"]]
      })

      expect(offer_data[:tags]).to eql(['cuenca', 'rio de janeiro', 'brasil'])
      expect(offer_data[:permalink]).to eql('vuelo-cuenca-rio-de-janeiro')
      expect(offer_data[:track_uri]).to eql("http://clk.tradedoubler.com/click?p(224466)a(2443327)g(20638346)url(http://partners.api.skyscanner.net/apiservices/referral/v1.0/ES/EUR/ES/CU/RI/2016-11-17/2016-12-02?apiKey=foo)")
      expect(offer_data[:landing_url]).to eql("http://partners.api.skyscanner.net/apiservices/referral/v1.0/ES/EUR/ES/CU/RI/2016-11-17/2016-12-02?apiKey=foo")
    end
  end

  describe "#evaluate" do
    it "creates new offer in cms if none found by tags" do
      allow(DSL::CMS).to receive(:find).exactly(2).times.and_return(nil)
      item = PriceItem.new(price_item_data)
      offer = Offer.new(item)

      # Important. Storing offer in mongo in order to have the id for the update_link
      mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
      allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
      offer.store_in_mongo

      res = offer.evaluate
      expect(res[:status]).to eql(:new)
      expect(res[:create_link]).to eql("http://localhost:4567/offer/1/create?offer_type=skyscanner")
    end

    it "returns found offer link" do
      allow(DSL::CMS).to receive(:find).with('offers', {"tags"=> {'$all': ['cuenca', 'rio de janeiro']}, "_visible"=>true}).exactly(1).times.and_return(nil)
      allow(DSL::CMS).to receive(:find).with("offers", {"tags"=> {'$all': ['cuenca', 'rio de janeiro', 'brasil']}, "_visible"=>true}).exactly(1).times.and_return([{'title'=> 'previous offer', '_id'=> 1}])

      item = PriceItem.new(price_item_data)
      offer = Offer.new(item)

      # Important. Storing offer in mongo in order to have the id for the update_link
      mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
      allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
      offer.store_in_mongo

      res = offer.evaluate
      expect(res[:status]).to eql(:found)
      expect(res[:update_link]).to eql("http://localhost:4567/offer/1/update/1?offer_type=skyscanner")
    end
  end
end

def stub_places
  place = instance_double("Place", id: 1, name: 'Cuenca', code: 'CU', city: 'Cuenca', country: 'Spain')
  place2 = instance_double("Place", id: 3, name: 'Río de Janeiro', code: 'RI', city: 'Río de Janeiro', country: 'Brasil')
  allow(Place).to receive(:get_data).with(42414).exactly(1).times.and_return(place)
  allow(Place).to receive(:get_data).with(79576).exactly(1).times.and_return(place2)
end
