# allow(DSL::CMS).to receive(:store_in_cms).with(
# 'offers',
# {
#     title: "Vuelo de Cuenca a Río de Janeiro ida y vuelta desde 119.0€",
#     teaser: "Viaja a Río de Janeiro con esta oferta de vuelo directo desde Cuenca por tan solo 119.0€ ida y vuelta. Entra para conocer las fechas y más detalles del vuelo",
#     description: "<h2>Detalles de la oferta de vuelo de Cuenca a Río de Janeiro desde 119.0€</h2><ul><li>Vuelo de ida de Cuenca a Río de Janeiro el 2016-11-17</li><li>Vuelo de regreso de Río de Janeiro a Cuenca el 2016-12-02</li><li>Vuelo con escala</li></ul>",
#     affiliate: "skyscanner",
#     affiliate_offer_id:  nil,
#     track_uri: "http://clk.tradedoubler.com/click?p(224466)a(2443327)g(20638346)url(http://partners.api.skyscanner.net/apiservices/referral/v1.0/ES/EUR/ES/CU/RI/2016-11-17/2016-12-02?apiKey=foo)",
#     landing_url: "http://partners.api.skyscanner.net/apiservices/referral/v1.0/ES/EUR/ES/CU/RI/2016-11-17/2016-12-02?apiKey=foo",
#     price: 119.0,
#     tags: ['cuenca', 'rio de janeiro', 'brazil'],
#     start_date: Date.today,
#     end_date: "2016-11-17T00:00:00",
#     _visible: false,
#     posted_at: Date.today
# }).exactly(1).times.and_return(true)
