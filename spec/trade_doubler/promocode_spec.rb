require_relative '../../trade_doubler/promocode'
require 'date'


RSpec.describe TradeDoubler, "#new promocode" do

  it "is properly initialized" do
    promo_data = {"id"=>157808, "programId"=>241330, "programName"=>"Groupalia ES", "code"=>"ARP2509NB", "updateDate"=>"1459366922307", "publishStartDate"=>"1421103600000", "publishEndDate"=>"1464731999999", "startDate"=>"1421103600000", "endDate"=>"1464731999999", "title"=>"5€ de descuento", "shortDescription"=>"Cupón de 5€, con un mínimo de 35€ de compra con el código ARP2509NB ", "description"=>"Cupón de 5€, con un mínimo de 35€ de compra con el código ARP2509NB ", "voucherTypeId"=>1, "defaultTrackUri"=>"http://clk.tradedoubler.com/click?a(2443327)p(241330)ttid(13)", "siteSpecific"=>false, "discountAmount"=>5.0, "isPercentage"=>false, "publisherInformation"=>"", "languageId"=>"es", "exclusive"=>false, "currencyId"=>"EUR", "logoPath"=>"http://hst.tradedoubler.com/file/241330/logos/logo_Groupalia_100.jpg"}
    promocode = TradeDoubler::Promocode.new(promo_data)

    expect(promocode[:code]).to eql("ARP2509NB")
    expect(promocode[:affiliate]).to eql('tradedoubler')
    expect(promocode[:affiliate_voucher_id]).to eql(157808)
    expect(promocode[:landing_url]).to eql(nil)
    expect(promocode[:track_uri]).to eql("http://clk.tradedoubler.com/click?a(2443327)p(241330)ttid(13)")
    expect(promocode[:title]).to eql("5€ de descuento")
    expect(promocode[:teaser]).to eql("Cupón de 5€, con un mínimo de 35€ de compra con el código ARP2509NB ")
    expect(promocode[:description]).to eql("Cupón de 5€, con un mínimo de 35€ de compra con el código ARP2509NB ")
  end

end