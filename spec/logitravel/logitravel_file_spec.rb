require_relative '../../logitravel/logitravel_file'

RSpec.describe Logitravel, "#extract_data" do
  it "retrieves and parses offers from json" do
    expect(Logitravel::LogitravelFile).to receive(:download).exactly(1).times.and_return('cruises.json')
    expect(Logitravel::LogitravelFile).to receive(:filepath).exactly(1).times.and_return(Dir.pwd + '/spec/logitravel/cruises.json')
    file = Logitravel::LogitravelFile.new(:cruise)
    json = file.extract_data
    expect(json.length).to eql(100)
  end
end