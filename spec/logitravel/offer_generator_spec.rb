require_relative '../../logitravel/offer_generator'
include Logitravel

RSpec.describe OfferGenerator, "#get_new_pack_sales" do

  it "returns offers ordered by price ASC" do
    # Avoid connecting to CMS or MongoDB
    allow(DSL::CMS).to receive(:set_conn).and_return(true)
    allow(DSL::CMS).to receive(:already_in_cms?).and_return(false)
    allow(DSL::CMS).to receive(:store_in_cms).and_return(true)
    allow(DSL::MongoDB).to receive(:set_conn).and_return(true)
    allow(DSL::MongoDB).to receive(:store).and_return(true)

    # Instances of Groupalia::Offer for such offers
    offer1 = instance_double("Logitravel::Offer", data: { landing_url: 'offer1', price: 5 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer2 = instance_double("Logitravel::Offer", data: { landing_url: 'offer2', price: 2 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer3 = instance_double("Logitravel::Offer", data: { landing_url: 'offer3', price: 4 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer4 = instance_double("Logitravel::Offer", data: { landing_url: 'offer4', price: 1 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer5 = instance_double("Logitravel::Offer", data: { landing_url: 'offer5', price: 3 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)

    script = OfferGenerator.new(:test)
    expect(script).to receive(:init_offers).and_return([offer1, offer2, offer3, offer4, offer5])
    offers = script.get_new_pack_sales
    expect(offers.size).to eql(5)
    offer = offers.first
    expect(offer[0].data[:price]).to eql(1)
    last_offer = offers.last
    expect(last_offer[0].data[:price]).to eql(5)
  end
end

RSpec.describe OfferGenerator, "#get_new_cruise_sales" do

  it "returns offers ordered by price ASC" do
    # Avoid connecting to CMS or MongoDB
    allow(DSL::CMS).to receive(:set_conn).and_return(true)
    allow(DSL::CMS).to receive(:already_in_cms?).and_return(false)
    allow(DSL::CMS).to receive(:store_in_cms).and_return(true)
    allow(DSL::MongoDB).to receive(:set_conn).and_return(true)
    allow(DSL::MongoDB).to receive(:store).and_return(true)

    # Instances of Groupalia::Offer for such offers
    offer1 = instance_double("Logitravel::Offer", data: { landing_url: 'offer1', price: 5 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer2 = instance_double("Logitravel::Offer", data: { landing_url: 'offer2', price: 2 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer3 = instance_double("Logitravel::Offer", data: { landing_url: 'offer3', price: 4 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer4 = instance_double("Logitravel::Offer", data: { landing_url: 'offer4', price: 1 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)
    offer5 = instance_double("Logitravel::Offer", data: { landing_url: 'offer5', price: 3 },  evaluate: { status: :new, create_link: '' }, store_in_cms: true, store_in_mongo: true)

    script = OfferGenerator.new(:test)
    expect(script).to receive(:init_offers).and_return([offer1, offer2, offer3, offer4, offer5])
    offers = script.get_new_cruise_sales
    expect(offers.size).to eql(5)
    offer = offers.first
    expect(offer[0].data[:price]).to eql(1)
    last_offer = offers.last
    expect(last_offer[0].data[:price]).to eql(5)
  end
end
