require_relative '../../logitravel/offer_generator'
require_relative '../../logitravel/offer_generator_utils'

include Logitravel
include Logitravel::OfferGeneratorUtils

RSpec.describe Logitravel, "#get_feed_offers" do  
  before(:example) do
    expect(Logitravel::LogitravelFile).to receive(:download).exactly(1).times.and_return('cruises.json')
    expect(Logitravel::LogitravelFile).to receive(:filepath).exactly(1).times.and_return(Dir.pwd + '/spec/logitravel/cruises.json')
    @file = Logitravel::LogitravelFile.new(:cruise)
    @json = @file.extract_data
  end

  it "gets offers from json and creates Logitravel::Offer instances" do
    file = instance_double("Logitravel::LogitravelFile", extract_data: @json)
    expect(Logitravel::LogitravelFile).to receive(:new).and_return(file)

    offers = init_offers(:feed, :cruise)
    expect(offers.size).to eql(100)
  end
end
