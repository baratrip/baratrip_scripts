require_relative '../../logitravel/offer'

describe "Website Offer" do

  context "cruise" do
    describe "#new" do
      it "is properly initialized" do

        cruise_name = "Maravilloso Mediterráneo II"
        cruise_company = "Costa Cruceros"
        cruise_ship = "Costa Diadema"
        duration = "8"
        price = "120"
        itinerary = "Barcelona, Palma de Mallorca, Civitavecchia (Roma), La Spezia (Florencia/Pisa), Savona (Italia), Marsella (Francia), Barcelona"
        departures = "06 febrero hasta 22 mayo"
        origin = "Barcelona"
        url = "http://www.logitravel.com/cruceros/mediterraneo-occidental/desde-barcelona/costa-diadema/maravilloso-mediterraneo-ii-16495569.html"

        offer_data = {
          cruise_name: cruise_name,
          cruise_company: cruise_company,
          cruise_ship: cruise_ship,
          origin: origin,
          departures: departures,
          itinerary: itinerary,
          duration: duration,
          price: price,
          url: url
        }

        ops = {
          offer_data: offer_data,
          offer_type: :cruise_sales,
          from: :site
        }

        offer = Logitravel::Offer.new(ops, :test)
        offer_data = offer.data
        expect(offer_data[:offer_type]).to eql(:cruise_sales)
        expect(offer_data[:offer_source]).to eql(:site)
        expect(offer_data[:landing_url]).to eql(url)
        expect(offer_data[:hash_url]).to eql("6a324e529ae47765911c48ec8deac28c5f6a0cb633af5b55e0dccac422043b23")
        expect(offer_data[:title]).to eql("Crucero 'Maravilloso Mediterráneo II' durante 8 días desde 120€")
        expect(offer_data[:seo_title]).to eql("Crucero Maravilloso Mediterráneo II desde 120€ – Baratrip.es")
        expect(offer_data[:teaser]).to eql("Pasa 8 días en este maravilloso crucero Maravilloso Mediterráneo II con salida desde Barcelona desde 120€. Más detalles aquí")
        expect(offer_data[:description]).to eql(nil)
        expect(offer_data[:meta_description]).to eql("Pasa 8 días en este maravilloso crucero Maravilloso Mediterráneo II con salida desde Barcelona desde 120€. Más detalles aquí")
        expect(offer_data[:permalink]).to eql("crucero-maravilloso-mediterraneo-ii")
        expect(offer_data[:tags]).to eql(["crucero", "barcelona", "palma de mallorca", "civitavecchia", "la spezia", "savona", "marsella"])
      end
    end
  end

  context "pack sales" do

    describe "#new" do
      it "is properly initialized" do
        offer_data = { origin: "Madrid", destination: "Tenerife" , duration: "8 días / 7 noches" , price: "120" , url: "http://www.logitravel.com/viajes/low-cost-espana-tenerife-15982595.html" }

        ops = {
          offer_data: offer_data,
          offer_type: :pack_sales,
          from: :site
        }

        offer = Logitravel::Offer.new(ops, :test)
        offer_data = offer.data
        expect(offer_data[:offer_type]).to eql(:pack_sales)
        expect(offer_data[:offer_source]).to eql(:site)
        expect(offer_data[:landing_url]).to eql("http://www.logitravel.com/viajes/low-cost-espana-tenerife-15982595.html")
        expect(offer_data[:hash_url]).to eql("56e27ecfb65e39313cdf50df477b12dbaa470952d2bfab3ffde77e1850ff349c")
        expect(offer_data[:title]).to eql("Vuelo más Hotel en Tenerife '8 días / 7 noches' desde 120€")
        expect(offer_data[:seo_title]).to eql("Vuelo más Hotel en Tenerife desde 120€ - Baratrip.es")
        expect(offer_data[:teaser]).to eql("Disfruta de 8 días / 7 noches días en Tenerife con esta oferta de vuelo más hotel en Tenerife desde tan solo 120€. Entra y conoce más detalles")
        expect(offer_data[:description]).to eql(nil)
        expect(offer_data[:meta_description]).to eql("Disfruta de 8 días / 7 noches días en Tenerife con esta oferta de vuelo más hotel en Tenerife desde tan solo 120€. Entra y conoce más detalles")
        expect(offer_data[:permalink]).to eql("vuelo-mas-hotel-tenerife")
        expect(offer_data[:tags]).to eql(["vuelohotel", "tenerife"])
      end
    end

    describe "#evaluate" do
      it "creates new offer in cms if none found by tags" do
        allow(DSL::CMS).to receive(:find).exactly(2).times.and_return([])
        offer_data = { origin: "Madrid", destination: "Tenerife" , duration: "8 días / 7 noches" , price: "120" , url: "http://www.logitravel.com/viajes/low-cost-espana-tenerife-15982595.html" }

        ops = {
          offer_data: offer_data,
          offer_type: :pack_sales,
          from: :site
        }

        offer = Logitravel::Offer.new(ops, :test)

        # Important. Storing offer in mongo in order to have the id for the update_link
        mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
        allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
        offer.store_in_mongo

        res = offer.evaluate
        expect(res[:status]).to eql(:new)
        expect(res[:create_link]).to eql("http://localhost:4567/offer/1/create?offer_type=logitravel")
      end

      it "returns found offer link" do
        allow(DSL::CMS).to receive(:find).with('offers', {"tags"=> {'$all': ['vuelohotel', 'tenerife']}, "_visible"=>true}).exactly(1).times.and_return([{'title'=> 'previous offer', '_id'=> 1}])

        offer_data = { origin: "Madrid", destination: "Tenerife" , duration: "8 días / 7 noches" , price: "120" , url: "http://www.logitravel.com/viajes/low-cost-espana-tenerife-15982595.html" }

        ops = {
          offer_data: offer_data,
          offer_type: :pack_sales,
          from: :site
        }

        offer = Logitravel::Offer.new(ops, :test)

        # Important. Storing offer in mongo in order to have the id for the update_link
        mongo_result = instance_double("Mongo::Operation::Write::Insert::Result", inserted_id: 1)
        allow(DSL::MongoDB).to receive(:store).exactly(1).times.and_return(mongo_result)
        offer.store_in_mongo

        res = offer.evaluate
        expect(res[:status]).to eql(:found)
        expect(res[:update_link]).to eql("http://localhost:4567/offer/1/update/1?offer_type=logitravel")
      end
    end
  end
end

describe "Feed offer"  do
  it "is properly initialized" do
    feed_offer = { "Id_text" => "142197" , "Name_text" => "Caribe Oriental" , "Description_text" => "Miami (EEUU), Philipsburg (St. Maarten), San Juan (Puerto Rico) , Nassau (Bahamas), Miami (EEUU)" , "Price_text" => "1309" , "Image_text" => "http://ssl.affiliate.logitravel.com/ts/i2795678/tsv?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fcdn.logitravel.com%2FcontenidosShared%2Fcruises%2Fship%2F544%2Fgeneric.jpg" , "Url_text" => "http://ssl.affiliate.logitravel.com/ts/i2795678/tsc?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fwww.logitravel.com%2Fcruisesshowcase%2FCruise%2FIndex%2F142197%3Fanuncio%3D83612647%26utm_medium%3D%23%7BCOMPANY_NAME%7D%26utm_source%3DAfiliado%26subid%3D%23%7BSUB_ID%7D"}

    ops = {
      offer_data: feed_offer,
      offer_type: :cruise,
      from: :feed
    }

    offer = Logitravel::Offer.new(ops, :test)
    # expect(offer[:landing_url]).to eql(nil)
    # expect(offer[:hash_url]).to eql(nil)
    expect(offer[:track_uri]).to eql("http://ssl.affiliate.logitravel.com/ts/i2795678/tsc?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fwww.logitravel.com%2Fcruisesshowcase%2FCruise%2FIndex%2F142197%3Fanuncio%3D83612647%26utm_medium%3D%23%7BCOMPANY_NAME%7D%26utm_source%3DAfiliado%26subid%3D%23%7BSUB_ID%7D")
    expect(offer.affiliate).to eql('logitravel')
    expect(offer.cms_model).to eql('offers')
    expect(offer.mongo_col).to eql(:logitravel_offers)
  end

  it "maps basic data" do
    feed_offer = { "Id_text" => "142197" , "Name_text" => "Caribe Oriental" , "Description_text" => "Miami (EEUU), Philipsburg (St. Maarten), San Juan (Puerto Rico) , Nassau (Bahamas), Miami (EEUU)" , "Price_text" => "1309" , "Image_text" => "http://ssl.affiliate.logitravel.com/ts/i2795678/tsv?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fcdn.logitravel.com%2FcontenidosShared%2Fcruises%2Fship%2F544%2Fgeneric.jpg" , "Url_text" => "http://ssl.affiliate.logitravel.com/ts/i2795678/tsc?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fwww.logitravel.com%2Fcruisesshowcase%2FCruise%2FIndex%2F142197%3Fanuncio%3D83612647%26utm_medium%3D%23%7BCOMPANY_NAME%7D%26utm_source%3DAfiliado%26subid%3D%23%7BSUB_ID%7D"}

    ops = {
      offer_data: feed_offer,
      offer_type: :cruise,
      from: :feed
    }

    offer = Logitravel::Offer.new(ops, :test)
    cms_data = offer.map_data_to_cms
    # expect(cms_data[:landing_url]).to eql(nil)
    expect(cms_data[:track_uri]).to eql("http://ssl.affiliate.logitravel.com/ts/i2795678/tsc?tst=!!TIME_STAMP!!&amc=aff.logitravel.22271.26105.40723&rmd=3&trg=http%3A%2F%2Fwww.logitravel.com%2Fcruisesshowcase%2FCruise%2FIndex%2F142197%3Fanuncio%3D83612647%26utm_medium%3D%23%7BCOMPANY_NAME%7D%26utm_source%3DAfiliado%26subid%3D%23%7BSUB_ID%7D")
    expect(cms_data[:affiliate]).to eql('logitravel')
  end
end
