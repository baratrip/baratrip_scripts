require_relative '../../common/utils'
require_relative '../../groupalia/groupalia'

include Common::Utils

describe "#group_public_offers" do
  it "correctly groups new offers" do
    offer1 = instance_double("Groupalia::Offer", data: { url: 'offer1', price: 100 }, already_in_mongo?: false, already_in_cms?: false, capture_web_data: true)
    offer2 = instance_double("Groupalia::Offer", data: { url: 'offer2', price: 100 }, already_in_mongo?: false, already_in_cms?: true, capture_web_data: true)
    offer3 = instance_double("Groupalia::Offer", data: { url: 'offer3', price: 100 }, already_in_mongo?: false, already_in_cms?: false, capture_web_data: true)

    expect(filter_published_offers([offer1, offer2, offer3])).to eq({ all: [offer1, offer2, offer3], new: [offer1, offer3], errors: []})
  end
end

describe "#sort by price" do
  it "correctly sorts" do
    offer1 = instance_double("Groupalia::Offer", data: { price: '1.000,53' })
    offer2 = instance_double("Groupalia::Offer", data: { price: '999,53' })

    expect(sort_by_price([offer1, offer2])).to eq([offer2, offer1])
  end
end

describe "#make_tag" do
  it "removes accents and other characters" do
    expect(make_tag('Río de Janeiro')).to eql('rio de janeiro')
  end
  it "removes parenthesis and inside content" do
    expect(make_tag(' livorno (florencia/pisa) ')).to eql('livorno')
    expect(make_tag('Tolón (Francia)')).to eql('tolon')
  end
end

describe "#price_to_f" do
  it "creates a float from different types of sources" do
    expect(price_to_f('39€')).to eql(39.0)
    expect(price_to_f('39,0')).to eql(39.0)
  end
end

describe "#track_tradedoubler_url" do
  it "creates a tracked url" do
    expect(track_tradedoubler_url('https://www.google.es', 1, 2)).to eql("http://clk.tradedoubler.com/click?p(1)a(2443327)g(2)url(https://www.google.es)")
  end
end

describe "#hash_url" do
  it "creates a hash from a url. always the same for same url" do
    url_1 = hash_url('https://www.google.es')
    url_2 = hash_url('https://www.google.es')
    url_3 = hash_url('https://www.google.es/')
    expect(url_1).to eql('980e6d55673b620b961740d581185e1b86c71d99ce5e8c96ca09ff55eb242d85')
    expect(url_1).to eql(url_2)
    expect(url_2).not_to eql(url_3)
  end
end
