require_relative '../skyscanner/api_client'
require_relative '../skyscanner/offer'
require_relative '../skyscanner/models/place'
require_relative '../skyscanner/models/price_list'

module Skyscanner

  module OfferGeneratorUtils

    def get_new_price_list(orig_country, to = 'anywhere')
      res = ApiClient.new().browse_quotes(orig_country, to)
      list = res["Quotes"]
      places = res["Places"]
      update_places(places)
      price_list = PriceList.new(orig_country, to, list)
      price_list.store
      return price_list
    end

    def update_places(places)
      places.each{ |place| Place.store(place) }
    end

    def evaluate_offers(env, list)
      offers_processed = []
      list.each do |item|
        begin
          offer = Offer.new(item, env)
          offer.store_in_mongo
          res = offer.evaluate
          offers_processed << [offer, res]
        rescue => e
          p e
          RollbarNotifier.error(e, item.data)
        end
      end
      offers_processed
    end

  end

end
