require_relative '../skyscanner/offer'

module Skyscanner

  class OfferHandler

    def initialize(env)
      DSL::CMS.set_conn(env)
      DSL::MongoDB.set_conn(env)
    end

    def update_cms_offer(mongo_id, cms_id)
      offer = restore_offer(mongo_id)
      offer.update_cms_offer(cms_id)
    end

    def create_cms_offer(mongo_id)
      offer = restore_offer(mongo_id)
      offer.store_in_cms
    end

    private

    def restore_offer(mongo_id)
      mongo_bson_id = BSON::ObjectId.from_string(mongo_id)
      Offer.restore(Offer::MONGO_COL, '_id', mongo_bson_id)
    end

  end

end
