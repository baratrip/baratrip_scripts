#require 'skyscanner'
require_relative '../common/dsl/dsl'
require_relative '../common/notification_manager'
require_relative '../skyscanner/offer_generator_utils'

module Skyscanner

  class OfferGenerator
    include OfferGeneratorUtils
    include NotificationManager

    def initialize(env)
      @env = env
      DSL::CMS.set_conn(env)
      DSL::MongoDB.set_conn(env)
      @report_to = report_to(env)
    end

    def get_daily_offers
      report = { price_list: [],	cheapest_list: []	}
      errors = []
      ['ES','FR','UK','NL'].each do |origin|
         begin
          countries_price_list = get_new_price_list(origin, 'anywhere')

          cheapest_list_always = countries_price_list.get_cheapest_list_always
          report[:cheapest_list] = evaluate_offers(@env, cheapest_list_always)

          cheapest_last_month_list = countries_price_list.get_cheapest_last_month
          cheapest_last_month_list = cheapest_last_month_list - cheapest_list_always
          report[:cheapest_last_month_list] = evaluate_offers(@env, cheapest_last_month_list)

          deliver_report("Skyscanner - Pricelist from #{origin} to anywhere, anytime", report)
        rescue => e
          msg = "Error for #{origin}"
          errors << msg
        end
      end
      p errors
    end

  end

  private

  def deliver_report(title, report)
    report = beauty_report(report)
    send_report( @report_to, title, report, [] )
  end

  def beauty_report(report)
    msg = beauty_list('Best Prices found this month', report[:cheapest_last_month_list])
    msg << beauty_list('Best Prices ever found', report[:cheapest_list])
    return msg
  end

  def beauty_list(name, list)
    msg = "<p><strong>#{name} (#{list.size})</strong></p>"
    list.each do |item|
      offer = item[0]
      result = item[1]
      msg << "<p>#{offer.price_item.destination.city} (#{offer.price_item.destination.country}): #{offer.data[:price]} <b>(#{result[:status].upcase})</b></p>"
      msg << "<p><span>Tracked url:</span> (#{offer.data[:track_uri]})</p>"
      msg << send("msg_for_#{result[:status]}", result)
      msg << "<p>------------------------------------------------------</p>"
    end
    return msg
  end

  def msg_for_new(result)
    "<p><span>Action:</span> <a href='#{result[:create_link]}'>>> Click here to Create <<</a></p>"
  end

  def msg_for_found(result)
    list = result[:cms_offer_data].map{|k,v| "<li>#{k}:&nbsp;&nbsp;#{v}</li>"}
    "<p>
      <span>Prev Offer Info:</span>
      <ul>
        #{list.join('')}
      </ul>
    </p>
    <p><span>Action:</span> <a href='#{result[:update_link]}'>>> Click here to Update <<</a></p>"
  end

end
