require_relative '../common/rollbar_notifier'
require_relative '../skyscanner/offer_generator'
require_relative '../skyscanner/offer_handler'

module Skyscanner
  include RollbarNotifier
end
