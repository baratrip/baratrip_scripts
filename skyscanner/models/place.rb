require_relative '../../common/dsl/dsl'

module Skyscanner
  class Place
    COLLECTION = 'skyscanner_places'

    attr_accessor :id, :code, :name, :country, :city

    def initialize(data)
      @id = data["id"]
      @place_type = data["place_type"]
      @code = data["code"]
      @name = data["name"]
      @country = data["country"]
      @city = data["city"]
    end

    def self.get_data(place_id)
      data = DSL::MongoDB.find(COLLECTION, 'id', place_id)
      raise "Place #{place_id} not found in mongo" if data.nil?
      Place.new(data)
    end

    def self.store(data)
      return if DSL::MongoDB.already_in_mongo?(COLLECTION, 'id', data["PlaceId"])
      DSL::MongoDB.store(COLLECTION, { id: data["PlaceId"], name: data["Name"], place_type: data["Type"], code: data["SkyscannerCode"], country: data["CountryName"], city: data["CityName"] })
    end

  end
end