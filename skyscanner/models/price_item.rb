require_relative '../../common/utils'

module Skyscanner

  class PriceItem
    COLLECTION = "skyscanner_quotes"
    attr_accessor :mongo_id, :price, :origin, :destination, :out_departure_date, :direct, :in_departure_date, :created_at

    def initialize(data)
      @price = data["MinPrice"]
      @direct = data["Direct"]
      @created_at = Date.today
      @updated_at = data["QuoteDateTime"]
      @origin = Place.get_data(data["OutboundLeg"]["OriginId"])
      @destination = Place.get_data(data["OutboundLeg"]["DestinationId"])
      @out_departure_date = data["OutboundLeg"]["DepartureDate"]
      @in_departure_date = data["InboundLeg"]["DepartureDate"]
    end

    def is_cheapest?
      return true if is_first_item_for_route?
      raise "No mongo_id for this item" if @mongo_id.nil?
      res = DSL::MongoDB.find_by_hash(COLLECTION, { 'origin_id': @origin.id, 'destination_id': @destination.id, 'price': { '$lte': @price }, '_id': { '$ne': @mongo_id } }).limit(1).to_a
      res.empty?
    end

    def is_cheapest_last_month?
      return true if is_first_item_for_route?
      raise "No mongo_id for this item" if @mongo_id.nil?
      res = DSL::MongoDB.find_by_hash(COLLECTION, { 'origin_id': @origin.id, 'destination_id': @destination.id, 'price': { '$lte': @price }, 'created_at': {'$gt': Date.today - 30, '$lte': Date.today }, '_id': { '$ne': @mongo_id } }).limit(1).to_a
      res.empty?
    end

    def is_first_item_for_route?
      total = DSL::MongoDB.find_by_hash(COLLECTION, { 'origin_id': @origin.id, 'destination_id': @destination.id }).limit(1).to_a
      total.empty?
    end

    def store
      # Then store item
      obj = DSL::MongoDB.store(COLLECTION, self.data)
      @mongo_id = obj.inserted_id
    end

    def data
      {
          price: @price,
          direct: @direct,
          created_at: @created_at,
          updated_at: @updated_at,
          origin_id: @origin.id,
          origin_name: @origin.name,
          destination_id: @destination.id,
          destination_name: @destination.name,
          out_departure_date: @out_departure_date,
          in_departure_date: @in_departure_date
      }
    end

  end
end