require_relative './price_item'
module Skyscanner
  class PriceList

    attr_accessor :list, :from, :to

    def initialize(from, to, quotes)
      @from = from
      @to = to
      quotes.reject!{ |item| item['MinPrice'].nil? }
      sorted_list = quotes.sort_by{|x| x["MinPrice"]}
      @list = sorted_list.collect{|x| item = PriceItem.new(x) }
    end

    def size
      @list.size
    end

    def get_cheapest_list_always
      cheapest_always = @list.reject{ |item| !item.is_cheapest? }
      cheapest_always
    end

    def get_cheapest_last_month
      cheapest_last_month = @list.reject{ |item| !item.is_cheapest_last_month? }
      cheapest_last_month
    end

    def store
      @list.each{|item| item.store }
      return true
    end

  end
end
