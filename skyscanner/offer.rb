require 'digest'
require 'open-uri'

require_relative '../common/offer'
require_relative '../skyscanner/api_client'

module Skyscanner

  class Offer < Common::Offer
    attr_accessor :price_item
    CMS_MODEL = 'offers'
    AFFILIATE = 'skyscanner'
    MONGO_COL = :skyscanner_offers

    def initialize(data, env = :development)
      @mongo_col = MONGO_COL
      @cms_model = CMS_MODEL
      @affiliate = AFFILIATE
      @env = env
      if data.class == BSON::Document
        @data = data
      else
        @price_item = data
        landing_url =  make_landing_url(@price_item)
        track_uri = track_tradedoubler_url(landing_url, 224466, 20638346)
        booking_url = make_booking_addition_url(@price_item)
        permalink = "vuelo-#{make_permalink(@price_item.origin.city)}-#{make_permalink(@price_item.destination.city)}"

        @data = {
            title: create_title(@price_item),
            teaser: create_teaser(@price_item),
            landing_url:  landing_url,
            track_uri:    track_uri,
            description: create_description(@price_item),
            seo_title:    "Vuelo de #{@price_item.origin.city} a #{@price_item.destination.city} desde #{@price_item.price}€ - Baratrip.es",
            meta_description: "Conoce #{@price_item.destination.country} gracias a este vuelo de #{@price_item.origin.city} a #{@price_item.destination.city} desde #{@price_item.price}€",
            price: @price_item.price,
            tags: get_tags(@price_item),
            tags_groups: get_tags_groups(@price_item),
            start_date: Date.today,
            end_date: @price_item.out_departure_date,
            booking_url: booking_url,
            permalink: permalink,
            categories: 'vuelos'
        }
      end
    end

    def evaluate
      cms_offers = []
      @data[:tags_groups][:offer].each do |tags|
        offers_found = self.class.find_by_tags(tags)
        cms_offers += offers_found if offers_found
      end
      if cms_offers.size > 0
        cms_offer = cms_offers[0]
        cms_offer_data = { edit_url: edit_offer_url(cms_offer['_id']) }
        ['_id', 'title', 'affiliate', 'price', 'posted_at', 'end_date', '_visible'].each{|attr| cms_offer_data[attr] = cms_offer[attr]}
        res = { status: :found, cms_offer_data: cms_offer_data, update_link: update_link(cms_offer['_id']) }
      else
        res = { status: :new, create_link: create_link }
      end
      res
    end

    def make_booking_addition_url(item)
      out_date = item.out_departure_date.split('T')[0]
      in_date = item.in_departure_date.split('T')[0]
      URI::encode("http://www.baratrip.es/hotel?destination=#{item.destination.name.downcase}&checkin=#{out_date}&checkout=#{in_date}")
    end

    def make_landing_url(item)
      out_date = item.out_departure_date.split('T')[0]
      in_date = item.in_departure_date.split('T')[0]
      ApiClient.new().make_landing_url(item.origin.code, item.destination.code, out_date, in_date)
    end

    def create_title(item)
      "Vuelo de #{item.origin.city} a #{item.destination.city} ida y vuelta desde #{item.price}€"
    end

    def create_teaser(item)
      "Viaja a #{item.destination.city} con esta oferta de vuelo directo desde #{item.origin.city} por tan solo #{item.price}€ ida y vuelta. Entra para conocer las fechas y más detalles del vuelo"
    end

    def create_description(item)
      out_date = item.out_departure_date.split('T')[0]
      in_date = item.in_departure_date.split('T')[0]
      desc = "<h2>Detalles de la oferta de vuelo de #{item.origin.city} a #{item.destination.city} desde #{item.price}€</h2>"
      desc += "<ul>"
      desc += "<li>Vuelo de ida de #{item.origin.city} a #{item.destination.city} el #{out_date}</li>"
      desc += "<li>Vuelo de regreso de #{item.destination.city} a #{item.origin.city} el #{in_date}</li>"
      desc += "<li>Vuelo #{item.direct ? 'directo' : 'con escala'}</li>"
      desc += "</ul>"
      desc
    end

    private

    def get_tags(item)
      o_city = make_tag(item.origin.city)
      d_city = make_tag(item.destination.city)
      d_country = make_tag(item.destination.country)
      [o_city, d_city, d_country]
    end

    def get_tags_groups(item)
      o_city = make_tag(item.origin.city)
      d_city = make_tag(item.destination.city)
      d_country = make_tag(item.destination.country)
      {
        post: [[o_city, d_city], [o_city, d_country]],
        offer: [[o_city, d_city, d_country], [o_city, d_city]]
      }
    end
  end
end
