require 'skyscanner'
module Skyscanner

  class ApiClient

    def initialize
      config = File.expand_path('../config/skyscanner.json', File.dirname(__FILE__))
      if File.exist?(config)
        @api_key = JSON.parse(File.read(config))['key']
        Skyscanner::Connection.apikey = @api_key
      else
        @api_key = 'foo'
      end
    end

    def browse_quotes(origin, destination)
      Connection.browse_quotes({ :country => "ES", :currency => "EUR", :locale => "es", :originPlace => origin, :destinationPlace => destination, :outboundPartialDate => "anytime", :inboundPartialDate => "anytime" })
    end

    def make_landing_url(o_code, d_code, o_date, r_date)
      "http://partners.api.skyscanner.net/apiservices/referral/v1.0/ES/EUR/ES/#{o_code}/#{d_code}/#{o_date}/#{r_date}?apiKey=#{@api_key}"
    end

  end

end
