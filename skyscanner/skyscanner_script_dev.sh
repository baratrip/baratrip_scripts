#!/bin/bash
# Get skyscanner today prices
STARTTIME=$(date +%s)
STARTTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$STARTTIME_P Launching skyscanner_importer"
ruby skyscanner_launcher.rb development "get_daily_offers"
ENDTIME=$(date +%s)
ENDTIME_P=$(date +%Y-%m-%d:%H:%M:%S)
echo "$ENDTIME_P Finished skyscanner_importer. Elapsed time $(($ENDTIME - $STARTTIME))"
