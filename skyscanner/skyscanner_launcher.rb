require_relative './skyscanner'
include Skyscanner
script = Skyscanner::OfferGenerator.new(ARGV[0])
methods = ARGV[1].split(',')
methods.each{|method| script.send(method)}
